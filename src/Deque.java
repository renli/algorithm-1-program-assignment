
import java.util.Iterator;




/**
 *
 * @author renli
 */
public class Deque<Item> implements Iterable<Item> {
   
   private int N;
   private Node first = null;
   private Node last = null;
   private Node old = null;
   
   private class Node {
        private Item item;
        private Node next;
        private Node previous;
    }
   
   public Deque() { 
       first = null;
       last = null;
       old = null;
   }                     
           
   public boolean isEmpty() {     // is the deque empty?
       return N == 0;
   }           
          
   public int size() {   // return the number of items on the deque
       return N;
   }
   
   public void addFirst(Item item) {     // insert the item at the front
       if (item != null) {
           old = first;
           first = new Node();
           first.item = item;
           first.next = old;
           if (old != null) {
                old.previous = first;
           }
           if (N == 0)
               last = first;
           N++;
       } else {
           throw new java.lang.NullPointerException();
       }
   }    
   
   
   public void addLast(Item item) {      // insert the item at the end
       if (item != null) {
           old = last;
           last = new Node();
           last.item = item;
           last.next = null;
           if (old != null) {
                old.next = last;
           }
           last.previous = old;
           if (N == 0)
               first = last;
           N++;
       } else {
           throw new java.lang.NullPointerException();
       }
   }
   
   public Item removeFirst() {           // delete and return the item at the front
       if (!isEmpty()) {
           Node result = first;
           if (first.next != null) {
                first = first.next;
                first.previous = null;
           } else {
                first = null;
                last = null;
           }
           N--;
           return result.item;
       } else {
           throw new java.util.NoSuchElementException();
       }
   }
   
   public Item removeLast() {            // delete and return the item at the end
       if (!isEmpty()) {
           Node result = last;
           
           if (last.previous != null) {
                last = last.previous;
                last.next = null;
           } else {
               first = null;
               last = null;
           }
            N--;
            return result.item;
       } else {
           throw new java.util.NoSuchElementException();
       }
   }
   
   
   @Override
   public Iterator<Item> iterator() { return new ListIterator(); }   // return an iterator over items in order from front to end
   
   private class ListIterator implements Iterator<Item> {
       
        private Node current = first;
        
        @Override
        public boolean hasNext() { return current != null; }
        
        @Override
        public void remove() { 
            throw new java.lang.UnsupportedOperationException(); 
        } 
        
        @Override
        public Item next() {
            
            if (current != null) {     
                    Item item = current.item;
                    current = current.next; 
                    return item;
            } else {
                throw new java.util.NoSuchElementException();
            }
        }
       
   }
       
}


