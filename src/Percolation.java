



/**
 *
 * @author renli
 */

public class Percolation {
   
   //int percolate[];
   private boolean open[];
   private int N;
   private QuickUnionUF qu;
   public Percolation(int N){              // create N-by-N grid, with all sites blocked
       
       qu = new QuickUnionUF(N*N + 2);
       open = new boolean[N*N + 1];             // align the index to qu
       for(int i=0; i<N; ++i)
	  for(int j=0; j<N; ++j){
              open[i*N + j + 1] = false;
              if(i == 0) qu.union(0, i*N + j + 1);          // connect to head 0
              if(i == N-1) qu.union(N*N + 1, i*N + j + 1);  // connect to tail N*N+1
          }
              
       this.N =N;
   }

   // open site (row i, column j) if it is not already, (1,1) is upper-left site
   public void open(int i, int j) throws IndexOutOfBoundsException{
       // index (i-1)*N + j
       if(!open[(i - 1)*N + j]){
           
           if(((i - 2)*N + j) >= 1)             // check upper site
               if(open[((i - 2)*N + j)]) qu.union((i-1)*N + j, (i - 2)*N + j);
           
           if((i*N + j) <= N*N)                 // check down site
               if(open[i*N + j]) qu.union((i-1)*N + j, i*N + j);
           
           
           if(((i - 1)*N + j + 1) <= N*N)       // check right site
               if(open[(i - 1)*N + j + 1]) qu.union((i-1)*N + j, (i - 1)*N + j + 1);
          
           
           if(((i - 1)*N + j - 1) >= 0)         // check left site
               if(open[(i - 1)*N + j - 1]) qu.union((i-1)*N + j, (i - 1)*N + j - 1);
               
           open[(i-1)*N + j] = true;
       }
   }
   
   // is site (row i, column j) open?
   public boolean isOpen(int i, int j){
       return open[(i - 1)*N + j];
   }
   
   // is site (row i, column j) full?
   public boolean isFull(int i, int j){
       return qu.connected((i-1)*N + j, 0);   // check if the site is connected to header
   }
   
   // does the system percolate?
   public boolean percolates(){
       return qu.connected(N*N + 1, 0);      // check if header is connected to tail
   }
   
}
