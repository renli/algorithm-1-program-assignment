



/**
 *
 * @author renli
 */
public class PercolationStats {
   
   private double result[];
   
   public PercolationStats(int N, int T){       // perform T independent computational experiments on an N-by-N grid
       result = new double[T];       
       for(int a=0; a<T; ++a){
           int i,j;
           int count = 0;
           Percolation p = new Percolation(N);
           while(!p.percolates()){
             do{
                 i = StdRandom.uniform(1, N+1);
                 j = StdRandom.uniform(1, N+1);
             }while(p.isOpen(i,j));
             p.open(i, j);
             count++;
           }
           result[a] = (double)count/(N*N);
       }
   }
   
   public double mean(){                    // sample mean of percolation threshold
       return StdStats.mean(result);
   }                     
   
   public double stddev(){                  // sample standard deviation of percolation threshold
       return StdStats.stddev(result);
   }                   
   
   public double confidenceLo(){            // returns lower bound of the 95% confidence interval
       double mean = mean();
       double dev = stddev()*stddev();
       double output = mean - (1.96*dev/Math.sqrt(result.length));
       return output;
   }             
   public double confidenceHi(){    // returns upper bound of the 95% confidence interval
       double mean = mean();
       double dev = stddev()*stddev();
       double output = mean + (1.96*dev/Math.sqrt(result.length));
       return output;
   }             


   // test client, described below
   public static void main(String[] args){
	int N = Integer.parseInt(args[0]);
	int T = Integer.parseInt(args[1]);
	PercolationStats P = new PercolationStats(N,T);
        StdOut.println("mean                    = " + P.mean());
        StdOut.println("stddev                  = " + P.stddev());
        StdOut.println("95% confidence interval = " + P.confidenceLo() + ", " + P.confidenceHi());
   }

}