
/**
 *
 * @author renli
 */

public class Subset {
    public static void main(String[] args) {
        
        int number = 0;
        if (args[0] != null) {
            number = Integer.parseInt(args[0]);
        }
        
        RandomizedQueue<String> rq = new RandomizedQueue();
        String input;
       
        while (!StdIn.isEmpty()) {
            input = StdIn.readString();
            rq.enqueue(input);
        }
        
        
        
        for (int j = 0; j < number; ++j) {
            StdOut.println(rq.dequeue());
        }
        
    }
}
