/*****************************************************************
 * ClassName: CorpusGenerator
 * 
 * Call: wsjGenerate(), to generate corpus for wsj
 * Generate corpus for topic model training
 *****************************************************************/

package com.renli.topic;

import static com.renli.ir.QueryExpension.removeStopword;
import com.renli.ir.TrecWSJIteratorFirst;
import com.renli.ir.TrecWSJIteratorSecond;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.en.EnglishAnalyzer;
import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.document.Document;
import org.apache.lucene.util.Version;

/**
 *
 * @author renli
 */
public class CorpusGenerator {
    
    private File[] files;
    private String output_url;
    private HashMap<String, String> map;
    
    public CorpusGenerator(String corpus_url, String output_url) {
        files = new File(corpus_url).listFiles();
        this.output_url = output_url;
        map = new HashMap<String, String>();
    }
    
    public void wsjGenerate() throws FileNotFoundException, IOException {
        Document document;
        PrintWriter out = new PrintWriter(new FileWriter(output_url, true));
        for (int i = 0; i<files.length; ++i) {
            if (files[i].getName().contains("87") || files[i].getName().contains("88") 
                                || files[i].getName().contains("89")) {
                TrecWSJIteratorFirst docs = new TrecWSJIteratorFirst(files[i]);         //TODO: refactor needed
                System.out.println("Construct First Iterator");                               
                while (docs.hasNext()) {
                    document = docs.next();
                    /* */
                    outToCorpus(document, out);
                }
            } else {
                TrecWSJIteratorSecond docs = new TrecWSJIteratorSecond(files[i]);                           
                System.out.println("Construct Second Iterator");                                
                while (docs.hasNext()) {
                    document = docs.next();
                    /* */   
                    outToCorpus(document, out);
                }
            }
            
        }
        
        //File mapSave = new File(Constants.OUTPUT_PATH + "/docno_text.hashmap.obj");
        //FileOutputStream fos = new FileOutputStream(mapSave);
        //ObjectOutputStream oos = new ObjectOutputStream(fos);
        //oos.writeObject(map);
        
    }
    
    private void outToCorpus(Document doc, PrintWriter out) throws FileNotFoundException, IOException {
        ArrayList<String> terms = new ArrayList();
        String[] title_terms = null;
        String[] text_terms = null;
        
        if (doc.get("title") != null)
            title_terms = removeStopword(doc.get("title").split(" "));
        if (doc.get("text") != null)
            text_terms = removeStopword(doc.get("text").split("[ \n\t]"));
        
        if(title_terms != null)
            terms.addAll(Arrays.asList(title_terms));
        if (text_terms != null)
            terms.addAll(Arrays.asList(text_terms));
        StringBuilder text = new StringBuilder();
        for(String t : terms){
            t = t.replaceAll("(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]", "");
            /* remove uncommon char and number */
            char[] chars = t.toCharArray();
            for(char c : chars){
                if((c >= 65 && c <= 90) || (c >= 97 && c <= 122)){
                    text.append(c);
                }
            }
            text.append(" ");
        }
        
        /* stemming */
        EnglishAnalyzer sa = new EnglishAnalyzer(Version.LUCENE_CURRENT);
        ArrayList<String> tokenlist = tokenizeString(sa, text.toString());
        text = new StringBuilder();
        for (String sample : tokenlist) {
            text.append(sample);
            text.append(" ");
        }
        
        //String docno = doc.get("docno");       
        //map.put(docno, text.toString());     
        //out.println(docno + '\t' + "X" + '\t' + text.toString());
    }
    
    
    
    public static ArrayList<String> tokenizeString(Analyzer analyzer, String string) {
        ArrayList<String> result = new ArrayList<String>();
        try {
        TokenStream stream  = analyzer.tokenStream(null, new StringReader(string));
        stream.reset();
        while (stream.incrementToken()) {
            result.add(stream.getAttribute(CharTermAttribute.class).toString());
        }
        } catch (IOException e) {
      // not thrown b/c we're using a string reader...
        throw new RuntimeException(e);
        }
            return result;
        }
    
    
    
    
    
    /* output the hashmap from docno to instance id */
    
    public static void generateMapping(String corpus_url, String output_url) throws IOException {
        HashMap<String, Integer> map = new HashMap<String, Integer>();
        File[] files = new File(corpus_url).listFiles();
        Document document;
        int counter = 0;
        PrintWriter out = new PrintWriter(new FileWriter(output_url, true));
        for (int i = 0; i<files.length; ++i) {
            if (files[i].getName().contains("87") || files[i].getName().contains("88") 
                                || files[i].getName().contains("89")) {
                TrecWSJIteratorFirst docs = new TrecWSJIteratorFirst(files[i]);         //TODO: refactor needed
                System.out.println("Construct First Iterator");                               
                while (docs.hasNext()) {
                    document = docs.next();
                    /* */
                    
                    map.put(document.get("docno"), counter);    System.out.println(document.get("docno") + "\t" + counter);
                    counter++;
                }
            } else {
                TrecWSJIteratorSecond docs = new TrecWSJIteratorSecond(files[i]);                           
                System.out.println("Construct Second Iterator");                                
                while (docs.hasNext()) {
                    document = docs.next();
                    /* */   
                    map.put(document.get("docno"), counter);    System.out.println(document.get("docno") + "\t" + counter);
                    counter++;
                }
            }
            
        }
        File objSave = new File(output_url);
        FileOutputStream fos = new FileOutputStream(objSave);
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(map);
    }
    
    
}
