
package com.renli.topic;

import cc.mallet.pipe.CharSequence2TokenSequence;
import cc.mallet.pipe.CharSequenceLowercase;
import cc.mallet.pipe.Pipe;
import cc.mallet.pipe.SerialPipes;
import cc.mallet.pipe.TokenSequence2FeatureSequence;
import cc.mallet.pipe.TokenSequenceRemoveStopwords;
import cc.mallet.pipe.iterator.CsvIterator;
import cc.mallet.topics.ParallelTopicModel;
import cc.mallet.types.Alphabet;
import cc.mallet.types.IDSorter;
import cc.mallet.types.Instance;
import cc.mallet.types.InstanceList;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Formatter;
import java.util.Iterator;
import java.util.Locale;
import java.util.TreeSet;
import java.util.regex.Pattern;

/**
 *
 * @author renli
 */
public class TopicModelTest {


    
    public static void applyLDA(String corpus_url, String output_model_url, String output_instance_url, int numOftopics ,int numOfIterations) throws FileNotFoundException, UnsupportedEncodingException, IOException {
        File file=new File(corpus_url);
        ArrayList<Pipe> pipeList = new ArrayList<Pipe>();
        pipeList.add( new CharSequenceLowercase() );
	pipeList.add( new CharSequence2TokenSequence(Pattern.compile("\\p{L}[\\p{L}\\p{P}]+\\p{L}")) );
	pipeList.add( new TokenSequenceRemoveStopwords(new File("stoplists/en.txt"), "UTF-8", false, false, false) );
	pipeList.add( new TokenSequence2FeatureSequence() );
        InstanceList instances = new InstanceList (new SerialPipes(pipeList));
	Reader fileReader = new InputStreamReader(new FileInputStream(file), "UTF-8");                              // throw UnsupportedEncodingException
	instances.addThruPipe(new CsvIterator (fileReader, Pattern.compile("^(\\S*)[\\s,]*(\\S*)[\\s,]*(.*)$"),
											   3, 2, 1));               // data, label, name fields
        ParallelTopicModel model = new ParallelTopicModel(numOftopics, 1.0, 0.01);
        model.addInstances(instances);
        model.setNumThreads(2);                  // use 2 threads
        model.setNumIterations(numOfIterations); // normally 1000-2000 iteration
	model.estimate();
        
        
        /* save and load test */
        System.out.println("Writing model and data...");
        File modelSave = new File(output_model_url);
        File instanceSave = new File(output_instance_url);
        FileOutputStream fos = new FileOutputStream(modelSave);
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(model);
        fos = new FileOutputStream(instanceSave);
        oos = new ObjectOutputStream(fos);
        oos.writeObject(instances);
        
    }
    
    public static void addInstanceTest() throws IOException {
        ArrayList<Pipe> pipeList = new ArrayList<Pipe>();
        pipeList.add( new CharSequenceLowercase() );
	pipeList.add( new CharSequence2TokenSequence(Pattern.compile("\\p{L}[\\p{L}\\p{P}]+\\p{L}")) );
	pipeList.add( new TokenSequenceRemoveStopwords(new File("stoplists/en.txt"), "UTF-8", false, false, false) );
	pipeList.add( new TokenSequence2FeatureSequence() );
        
        InstanceList instances = new InstanceList (new SerialPipes(pipeList));
        instances.addThruPipe(new Instance("Rogers Corp Rights Plan Rogers Corp maker parts electronics industry board approved "
                + "rights plan designed protect holders event hostile takeover attempt "
                + "The company aware takeover interest Rogers "
                + "rights circumstances entitle holders buy shares Rogers surviving company half price", "X", "WSJ870323-0174", ""));
        instances.addThruPipe(new Instance("Texas Instruments Expansion Texas Instruments Inc expects begin construction squarefoot plant "
                + "month Denton Texas The company plant completed late employ people The plant Texas Instruments Defense Systems "
                + "Electronics Group produce electronic equipment", "X", "WSJ870323-0172", ""));
        instances.addThruPipe(new Instance("Canadian Firms New Orders Canadian manufacturers orders fell  billion Canadian January  Decembers "
                + "billion seasonally adjusted basis Statistics Canada federal agency said The decrease  increase December "
                + "Manufacturers shipments trend falling  January  billion  increase previous month The agency some indication upturn "
                + "recent irregular pattern shipments generally downward trend", "X", "WSJ870323-0178", ""));
        
        
        ParallelTopicModel model = new ParallelTopicModel(10, 1.0, 0.01);
        model.setNumIterations(50);
        model.addInstances(instances);
        model.setNumThreads(2);
        model.estimate();
        
        int numTopics = model.getNumTopics();
        Alphabet dataAlphabet = instances.getDataAlphabet();        // The data alphabet maps word IDs to strings
        double[] topicDistribution = model.getTopicProbabilities(0);
        ArrayList<TreeSet<IDSorter>> topicSortedWords = model.getSortedWords();
        for (int topic = 0; topic < numTopics; topic++) {
            Iterator<IDSorter> iterator = topicSortedWords.get(topic).iterator();		
			Formatter out = new Formatter(new StringBuilder(), Locale.US);
			out.format("%d\t%.3f\t", topic, topicDistribution[topic]);
			int rank = 0;
			while (iterator.hasNext() && rank < 5) {
				IDSorter idCountPair = iterator.next();
				out.format("%s (%.3f) ", dataAlphabet.lookupObject(idCountPair.getID()), idCountPair.getWeight());
				rank++;
			}
			System.out.println(out);
        }
        
    }
    
    public static void loadTest(String model_url, String instance_url, int instanceNumber) throws FileNotFoundException, IOException, ClassNotFoundException {
        
        /* read object */
        File modelSave = new File(model_url);
        File instanceSave = new File(instance_url);
        FileInputStream fis = new FileInputStream(modelSave);
        ObjectInputStream ois = new ObjectInputStream(fis);
        ParallelTopicModel model = (ParallelTopicModel) ois.readObject();
        fis = new FileInputStream(instanceSave);
        ois = new ObjectInputStream(fis);
        InstanceList instances = (InstanceList) ois.readObject();
        int numTopics = model.getNumTopics();
        
        Alphabet dataAlphabet = instances.getDataAlphabet();        // The data alphabet maps word IDs to strings
        double[] topicDistribution = model.getTopicProbabilities(instanceNumber);
        ArrayList<TreeSet<IDSorter>> topicSortedWords = model.getSortedWords();
        for (int topic = 0; topic < numTopics; topic++) {
			Iterator<IDSorter> iterator = topicSortedWords.get(topic).iterator();		
			Formatter out = new Formatter(new StringBuilder(), Locale.US);
			out.format("%d\t%.3f\t", topic, topicDistribution[topic]);
			int rank = 0;
			while (iterator.hasNext() && rank < 5) {
				IDSorter idCountPair = iterator.next();
				out.format("%s (%.3f) ", dataAlphabet.lookupObject(idCountPair.getID()), idCountPair.getWeight());
				rank++;
			}
			System.out.println(out);
	}
    }
    
    
}
