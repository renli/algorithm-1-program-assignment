
package com.renli.topic;

import java.io.Serializable;

/**
 *
 * @author renli
 */

public class TopicDistribution implements Serializable{
    
    public double p;
    public int sum;
    
    public TopicDistribution(double p, int sum) {
        this.p = p;
        this.sum = sum;
    }
}