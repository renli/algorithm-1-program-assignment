
package com.renli.topic;

import cc.mallet.topics.ParallelTopicModel;
import cc.mallet.types.Alphabet;
import cc.mallet.types.IDSorter;
import cc.mallet.types.InstanceList;
import com.renli.util.Constants;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.TreeSet;
import org.apache.lucene.index.DirectoryReader;

import org.apache.lucene.index.IndexReader;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;

/**
 *
 * @author renli
 */
public class TopicModelUser {
    
    private ParallelTopicModel model;
    private InstanceList instances;
    private ArrayList<TreeSet<IDSorter>> topicSortedWords;
    private Alphabet dataAlphabet;
    private HashMap<String, Integer> docnoInstanceIDMap;
    private HashMap<String, String> docnoTextMap;
    private int numOfTopic;
    private IndexReader reader;
    private HashMap<Integer, HashMap<String, Double>> topicMap;
    private HashMap<Integer, Integer> sumMap;
    
    public TopicModelUser(String model_url, String instance_url, String instanceHashmap_url, String textHashmap_url, 
            String topicHashmap_url, String sumHashMap_url) throws FileNotFoundException, IOException, ClassNotFoundException {
        File modelSave = new File(model_url);
        File instanceSave = new File(instance_url);
        FileInputStream fis = new FileInputStream(modelSave);
        ObjectInputStream ois = new ObjectInputStream(fis);
        model = (ParallelTopicModel) ois.readObject();
        fis = new FileInputStream(instanceSave);
        ois = new ObjectInputStream(fis);
        instances = (InstanceList) ois.readObject();
        numOfTopic = model.getNumTopics();
        topicSortedWords = model.getSortedWords();
        dataAlphabet = instances.getDataAlphabet();
                
        Directory dir = FSDirectory.open(new File(Constants.INDEX_PATH));
        reader = DirectoryReader.open(dir);
        
        //read instance hashmap docno -> document instance number
        File hashMap = new File(instanceHashmap_url);
        fis = new FileInputStream(hashMap);
        ois = new ObjectInputStream(fis);
        docnoInstanceIDMap = (HashMap<String, Integer>) ois.readObject();   
      
        //read text hashmap docno -> text field	     
        hashMap = new File(textHashmap_url);
        fis = new FileInputStream(hashMap);
        ois = new ObjectInputStream(fis);
        docnoTextMap = (HashMap<String, String>) ois.readObject();
        
        // read topic distribution hash topicID -> word distribution
        hashMap = new File(topicHashmap_url);
        fis = new FileInputStream(hashMap);
        ois = new ObjectInputStream(fis);
        topicMap = (HashMap<Integer, HashMap<String, Double>>) ois.readObject();
        
        // read topic sum hash topicID -> sum of instances
        hashMap = new File(sumHashMap_url);
        fis = new FileInputStream(hashMap);
        ois = new ObjectInputStream(fis);
        sumMap = (HashMap<Integer, Integer>) ois.readObject();     
    }
    
    
    /**
     *
     * @param query
     * @param docnos
     * @return
     * @throws IOException
     * sample version of lda-rm, count only first few documents retrieved.
     * return extended list
     */
    public ArrayList<String> ldaRMsample(String query, ArrayList<String> docnos) throws IOException {
        
        /* sort top k topics of query*/
        ArrayList<TopicID> topics = new ArrayList<TopicID>();        
        for (int i = 0; i < numOfTopic; ++i) {
            topics.add(new TopicID(i, pqt(query, i)));
        }
        Collections.sort(topics, Collections.reverseOrder());                           // deceding order
        ArrayList<TopicID> top_topics = new ArrayList<TopicID>(topics.subList(0, 20));   // top k topics
                                                                                                              
        /* read topic top x words */
        ArrayList<extendWord> extendWords = new ArrayList<extendWord>();
        for (TopicID t : top_topics) {
            Iterator<IDSorter> iterator = topicSortedWords.get(t.getTopicID()).iterator();
            int rank = 0;
	    while (iterator.hasNext() && rank < 20) {       // top m words
                IDSorter idCountPair = iterator.next();
                String term = dataAlphabet.lookupObject(idCountPair.getID()).toString();    
                extendWords.add(new extendWord(term, pwq_sample_smoothing(term, query, docnos, top_topics)));    //add to top k: Nov. 11
             // extendWords.add(new extendWord(term, pwd_rm(term, query, docnos, top_topics)));
                rank++;
            }
        }
        Collections.sort(extendWords, Collections.reverseOrder());  
        ArrayList<String> result = new ArrayList<String>();
        /* remove redundance and pick top 5 */
        int count = 0;
        extendWord redundance = new extendWord();
        Iterator<extendWord> iterator = extendWords.iterator();        
        while (count < 5 && iterator.hasNext()) {
            extendWord word = iterator.next();
            if (!word.getTerm().equalsIgnoreCase(redundance.getTerm()) && !query.contains(word.getTerm())) {
                if (word.getWQ() >= 0.01) {   // threshold
                    result.add(word.getTerm());     System.out.println(word.getTerm() + " " + word.getWQ());
                    redundance = word;
                } else {
                    break;
                }
                count++;
            }
        }
        return result;
    }
    
    

    /**
     *
     * @param term
     * @param query
     * @param docnos
     * @param topicIDs
     * @return P(w|q) 
     * @throws IOException sample version of pwq, count only first few documents retrieved. 
     */
    public double pwq_sample(String term, String query, ArrayList<String> docnos, ArrayList<TopicID> topicIDs) throws IOException {      // add to top k: Nov. 11
        int totalNumOfDocument = docnos.size();
        double sum = 0;
        for (int i = 0; i < totalNumOfDocument; ++i) {                  
            String docno = docnos.get(i);
            sum += pwdq_topk(term, docno, query, topicIDs) * pdq(docnos, docno, query);    // * (1 / (double) totalNumOfDocument);  //// add to top k: Nov. 11
        }
        return sum;
    } 
    
    
    /**
     *
     * @param term
     * @param query
     * @param docnos
     * @param topicIDs
     * @return
     * @throws IOException
     */
    public double pwq_sample_smoothing(String term, String query, ArrayList<String> docnos, ArrayList<TopicID> topicIDs) throws IOException {
        int totalNumOfDocument = docnos.size();
        double sum = 0;
        for (int i = 0; i < totalNumOfDocument; ++i) {                  
            String docno = docnos.get(i);
            sum += (0.6 * pwd(term, docno)  + 0.4 * pwdq_topk(term, docno, query, topicIDs)) * pdq(docnos, docno, query);   
        }
        return sum;
    }
    
    /* appy rm without topic */
    /**
     *
     * @param term
     * @param query
     * @param docnos
     * @param topicIDs
     * @return
     * @throws IOException
     */
    public double pwd_rm(String term, String query, ArrayList<String> docnos, ArrayList<TopicID> topicIDs) throws IOException {
        int totalNumOfDocument = docnos.size();
        double sum = 0;
        for (int i = 0; i < totalNumOfDocument; ++i) { 
            String docno = docnos.get(i);
            sum += pwd(term, docno) * pdq(docnos, docno, query);
        }
        return sum;
    }
    
    
    /**
     *
     * @param term
     * @param query
     * @param docnos
     * @return
     * @throws IOException
     */
    public double pwq_sample(String term, String query, ArrayList<String> docnos) throws IOException {      // change to top k: Nov. 11
        int totalNumOfDocument = docnos.size();
        double sum = 0;
        for (int i = 0; i < totalNumOfDocument; ++i) {                  
            String docno = docnos.get(i);
            sum += pwdq(term, docno, query) * pdq(docnos, docno, query);  //* (1 / (double) totalNumOfDocument);  //optimazed
        }
        return sum;
    }
    
    
    
    /* tested, take too much time  ==================================!!!
    * P(w|q)
    */
    private double pwq(String term, String query) throws IOException {
        int totalNumOfDocument = reader.numDocs();
        double sum = 0;
        for (int i = 0; i < totalNumOfDocument; ++i) {          // need optimazation, totalNumOfDocument is too large            
            String docno = reader.document(i).get("docno");
           // sum += pwdq(term, docno, query) * pdq(docnos, docno, query);  // *(1 / (double) totalNumOfDocument);
        }
        return sum;
    }
    

    /**
     *
     * @param docnos
     * @param docno
     * @param query
     * @return P(D|q)
     * @throws IOException
     */
    public double pdq(ArrayList<String> docnos, String docno, String query) throws IOException {
        double evidence = 0;
        double joint = 0;
        double numOfDocument = docnos.size();
        for (String d : docnos) {
            evidence += pqd(query, d) * (1 / (double) numOfDocument);
        }
        joint = pqd(query, docno) * (1 / (double) numOfDocument);
        
        return joint / evidence;
    }
    
    

    /**
     * 
     * @param term
     * @param docno
     * @param query
     * @return P(w|Di, q)
     * @throws IOException
     */
    public double pwdq(String term, String docno, String query) throws IOException {
        double sum = 0;
        for (int i = 0; i < numOfTopic; ++i) {
            double p = pwt(term, i) * (ptd(i, docno) * pqt(query, i) / pqd(query, docno));
            sum += p;
        }
        
        return sum;
    }
    
    
    /**
     *
     * @param term
     * @param docno
     * @param query
     * @param topicIDs
     * @return P(w|Di, q) with top k documents
     * @throws IOException
     */
    public double pwdq_topk(String term, String docno, String query, ArrayList<TopicID> topicIDs) throws IOException {
        double sum = 0;
        for (TopicID topicID : topicIDs) {
            double p = pwt(term, topicID.getTopicID()) * (ptd(topicID.getTopicID(), docno) 
                    * pqt(query, topicID.getTopicID()) / pqd(query, docno));
            sum += p;
        }      
        return sum;
    }
    
    

    /**
     *
     * @param q
     * @param topicID
     * @return P(q|t) q has to be under stemming
     */
    public double pqt(String q, int topicID) {
        String[] terms = q.split(" ");

        if (terms != null) {
            double[] weights = new double[terms.length];
            for (int i = 0; i < terms.length; ++i) weights[i] = 0;  // initial
            int topicSum = sumMap.get(topicID);
                for (int i = 0; i < weights.length; ++i) {
                     if (topicMap.get(topicID).get(terms[i]) != null)
                        weights[i] = topicMap.get(topicID).get(terms[i]);              
                }
            
            double pro = 1;
            
            for (int i = 0; i < terms.length; ++i) {
                if (weights[i] > 0 && !terms[i].equalsIgnoreCase(" ")) {
                    pro = pro * weights[i];
                } else {
                    pro = pro * (1 / topicSum);
                }
            }              
            return pro * numOfOrders(terms.length);
        }
        return 0;
    }
    

    /**
     *
     * @param term
     * @param topicID
     * @return P(w|t)
     */
    public double pwt(String term, int topicID) {
        
        int topicSum = sumMap.get(topicID);
        double weight = 0;
        
        if (topicMap.get(topicID).get(term) != null)
            weight = topicMap.get(topicID).get(term);
        
        if (weight == 0) weight = 1/(double)topicSum;
        return weight;
    }
    
    

    /**
     *
     * @param topicID
     * @param docno
     * @return P(t|D)
     */
    public double ptd(int topicID, String docno) {
        int documentID = docnoInstanceIDMap.get(docno);
        double[] topicDistribution = model.getTopicProbabilities(documentID);
        return topicDistribution[topicID];
    }
    

    /**
     *
     * @param q
     * @param docno
     * @return P(q|D) q has to be under stemming
     * @throws IOException
     */
    public double pqd(String q, String docno) throws IOException {
        int documentID = docnoInstanceIDMap.get(docno);
        String[] termList = docnoTextMap.get(docno).split(" ");
        String[] queryTermList = q.split(" ");
        double pro = 1;
        for (String queryTerm : queryTermList) {
            pro = pro * (tf(queryTerm, termList) / (double) termList.length);
        }
        return pro * numOfOrders(queryTermList.length);
    }
    
    
    /**
     *
     * @param term
     * @param docno
     * @return
     */
    public double pwd(String term, String docno) {
        int documentID = docnoInstanceIDMap.get(docno);
        String[] termList = docnoTextMap.get(docno).split(" ");
        double pro = tf(term, termList) / (double) termList.length;
        return pro;
    }
    
    
    /* return term frequency */
    private double tf(String term, String[] termlist) {
        int tf = 0;
        for (int i = 0; i < termlist.length; ++i) {
            if (termlist[i].equalsIgnoreCase(term)) tf++;
        }      
        return tf > 0 ? tf : 1;
    }
    
    /* don't use constants of multinorminal since query don't have same term */
    private int numOfOrders(int n) {
        if (n <= 0) return 1;
        int result = 1;
        for (int i = 1; i <= n; ++i) {
            result *= i;
        }
        return result;       
    }
    
    
    private class TopicID implements Comparable<TopicID> {
        private int topicID;
        private double pqt;
        
        public TopicID(int topicID, double pqt) {
            this.pqt = pqt;
            this.topicID = topicID;
        }
        
        public int getTopicID() {return topicID;}
        public double getPqt() {return pqt;}
        
        @Override
        public int compareTo(TopicID that) {
            if (this.pqt > that.pqt) return 1;
            if (this.pqt < that.pqt) return -1;
            return +0;
        }
    }
    
    private class extendWord implements Comparable<extendWord> {
        
        private String term;
        private double wq;
        
        public extendWord() {
            term = " ";
            wq = 0.0;
        }
        
        public extendWord(String term, double wq) {
            this.term = term;
            this.wq = wq;
        }
        
        public String getTerm() {
            return term;
        }
        
        public double getWQ() {
            return wq;
        }
        
        public int compareTo(extendWord that) {
            if (this.wq > that.wq) return 1;
            if (this.wq < that.wq) return -1;
            return +0;
        } 
    } 
    
}
