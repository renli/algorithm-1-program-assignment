
package com.renli.topic;

import cc.mallet.pipe.CharSequence2TokenSequence;
import cc.mallet.pipe.CharSequenceLowercase;
import cc.mallet.pipe.Pipe;
import cc.mallet.pipe.SerialPipes;
import cc.mallet.pipe.TokenSequence2FeatureSequence;
import cc.mallet.pipe.TokenSequenceRemoveStopwords;
import cc.mallet.topics.ParallelTopicModel;
import cc.mallet.types.Alphabet;
import cc.mallet.types.IDSorter;
import cc.mallet.types.Instance;
import cc.mallet.types.InstanceList;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.TreeSet;
import java.util.regex.Pattern;

/**
 *
 * @author renli
 */
public class LDARMUser {
    
    private HashMap<String, String> docnoTextMap;
    private HashMap<String, Integer> docnoInstanceIDMap;
    private ArrayList<String> docnos;
    private ParallelTopicModel model;
    private InstanceList instances;
    private int numOfTopic;
    private Alphabet dataAlphabet;
    private ArrayList<TreeSet<IDSorter>> topicSortedWords;
    
    public LDARMUser(String textHashmap_url, ArrayList<String> docnos, int numOftopics, int numOfIterations) 
            throws FileNotFoundException, IOException, ClassNotFoundException {
        File hashMap = new File(textHashmap_url);
        FileInputStream fis = new FileInputStream(hashMap);
        ObjectInputStream ois = new ObjectInputStream(fis);
        docnoTextMap = (HashMap<String, String>) ois.readObject();
        this.docnos = docnos;        
        docnoInstanceIDMap = new HashMap<String, Integer>();
                
        /* add text to instance*/
        ArrayList<Pipe> pipeList = new ArrayList<Pipe>();
        pipeList.add( new CharSequenceLowercase() );
	pipeList.add( new CharSequence2TokenSequence(Pattern.compile("\\p{L}[\\p{L}\\p{P}]+\\p{L}")) );
	pipeList.add( new TokenSequenceRemoveStopwords(new File("stoplists/en.txt"), "UTF-8", false, false, false) );
	pipeList.add( new TokenSequence2FeatureSequence() );
        instances = new InstanceList (new SerialPipes(pipeList));
        int index = 0;
        for (String docno : docnos) {
            String text = docnoTextMap.get(docno);
            instances.addThruPipe(new Instance(text, "X", docno, ""));
            docnoInstanceIDMap.put(docno, index);
            index++;
        }
        
        model = new ParallelTopicModel(numOftopics, 1.0, 0.01);
        model.addInstances(instances);
        model.setNumThreads(2);                  // use 2 threads
        model.setNumIterations(numOfIterations); // normally 1000-2000 iteration
        model.setTopicDisplay(1000, 0);
	model.estimate();  
        numOfTopic = model.getNumTopics();
        dataAlphabet = instances.getAlphabet();
        topicSortedWords = model.getSortedWords();
    }
    
    /**
     *
     * @param query
     * @param docnos
     * @return
     * @throws IOException
     * sample version of lda-rm, count only first few documents retrieved.
     * return extended list
     */
    public ArrayList<String> ldaRMsample(String query) throws IOException {
        
        /* sort top k topics of query*/
        ArrayList<TopicID> topics = new ArrayList<TopicID>();      
        for (int i = 0; i < numOfTopic; ++i) {
            topics.add(new TopicID(i, pqt(query, i)));
        }
        Collections.sort(topics, Collections.reverseOrder());                           // deceding order
        ArrayList<TopicID> top_topics = new ArrayList<TopicID>(topics.subList(0, 20));   // top k topics
              
         /* read topic top x words */
        ArrayList<TreeSet<IDSorter>> topicSortedWords = model.getSortedWords();
        ArrayList<extendWord> extendWords = new ArrayList<extendWord>();
        for (TopicID t : top_topics) {
            Iterator<IDSorter> iterator = topicSortedWords.get(t.getTopicID()).iterator();
            int rank = 0;
	    while (iterator.hasNext() && rank < 100) {                                  // top m words
                IDSorter idCountPair = iterator.next();
                String term = dataAlphabet.lookupObject(idCountPair.getID()).toString();    
                extendWords.add(new extendWord(term, pwq_sample_smoothing(term, query, docnos, top_topics)));    //add to top k: Nov. 11
                rank++;
            }
        }
        Collections.sort(extendWords, Collections.reverseOrder());  
        ArrayList<String> result = new ArrayList<String>();
        /* remove redundance and pick top 5 */
        int count = 0;
        extendWord redundance = new extendWord();
        Iterator<extendWord> iterator = extendWords.iterator();        
        while (count < 10 && iterator.hasNext()) {
            extendWord word = iterator.next();
            if (!word.getTerm().equalsIgnoreCase(redundance.getTerm()) && !query.contains(word.getTerm())) {
                if (word.getWQ() > 0) {                         // threshold
                    result.add(word.getTerm());     System.out.println(word.getTerm() + " " + word.getWQ());
                    redundance = word;
                } else {
                    break;
                }
                count++;
            }
        }
        return result;
    }
    

    
        /**
     *
     * @param term
     * @param query
     * @param docnos
     * @param topicIDs
     * @return
     * @throws IOException
     */
    public double pwq_sample_smoothing(String term, String query, ArrayList<String> docnos, ArrayList<TopicID> topicIDs) throws IOException {
        int totalNumOfDocument = docnos.size();
        double sum = 0;
        for (int i = 0; i < totalNumOfDocument; ++i) {                  
            String docno = docnos.get(i);
            sum += (0.5 * pwd(term, docno)  + 0.5 * pwdq_topk(term, docno, query, topicIDs)) * pdq(docnos, docno, query);   
        }
        return sum;
    }
    
       /* appy rm without topic */
    /**
     *
     * @param term
     * @param query
     * @param docnos
     * @param topicIDs
     * @return
     * @throws IOException
     */
    public double pwd_rm(String term, String query, ArrayList<String> docnos, ArrayList<TopicID> topicIDs) throws IOException {
        int totalNumOfDocument = docnos.size();
        double sum = 0;
        for (int i = 0; i < totalNumOfDocument; ++i) { 
            String docno = docnos.get(i);
            sum += pwd(term, docno) * pdq(docnos, docno, query);
        }
        return sum;
    }
    
    
    /**
     *
     * @param term
     * @param query
     * @param docnos
     * @return
     * @throws IOException
     */
    public double pwq_sample(String term, String query, ArrayList<String> docnos) throws IOException {      // change to top k: Nov. 11
        int totalNumOfDocument = docnos.size();
        double sum = 0;
        for (int i = 0; i < totalNumOfDocument; ++i) {                  
            String docno = docnos.get(i);
            sum += pwdq(term, docno, query) * pdq(docnos, docno, query); 
        }
        return sum;
    }
    
    
    /**
     *
     * @param docnos
     * @param docno
     * @param query
     * @return P(D|q)
     * @throws IOException
     */
    public double pdq(ArrayList<String> docnos, String docno, String query) throws IOException {
        double evidence = 0;
        double joint = 0;
        double numOfDocument = docnos.size();
        for (String d : docnos) {
            evidence += pqd(query, d) * (1 / (double) numOfDocument);
        }
        joint = pqd(query, docno) * (1 / (double) numOfDocument);
        
        return joint / evidence;
    }
    
    

    /**
     * 
     * @param term
     * @param docno
     * @param query
     * @return P(w|Di, q)
     * @throws IOException
     */
    public double pwdq(String term, String docno, String query) throws IOException {
        double sum = 0;
        for (int i = 0; i < numOfTopic; ++i) {
            double p = pwt(term, i) * (ptd(i, docno) * pqt(query, i) / pqd(query, docno));
            sum += p;
        }
        
        return sum;
    }
    
    
    /**
     *
     * @param term
     * @param docno
     * @param query
     * @param topicIDs
     * @return P(w|Di, q) with top k documents
     * @throws IOException
     */
    public double pwdq_topk(String term, String docno, String query, ArrayList<TopicID> topicIDs) throws IOException {
        double sum = 0;
        for (TopicID topicID : topicIDs) {
            double p = pwt(term, topicID.getTopicID()) * (ptd(topicID.getTopicID(), docno) 
                    * pqt(query, topicID.getTopicID()) / pqd(query, docno));
            sum += p;
        }      
        return sum;
    }
    
    

    /**
     *
     * @param q
     * @param topicID
     * @return P(q|t) q has to be under stemming
     */
    public double pqt(String q, int topicID) {
        String[] terms = q.split(" ");

        if (terms != null) {
            double[] weights = new double[terms.length];
            for (int i = 0; i < terms.length; ++i) 
                weights[i] = 0;  // initial
            
            /* calculate topic sum */
            Iterator<IDSorter> iterator = topicSortedWords.get(topicID).iterator();
            double topicSum = 0;
            
            while (iterator.hasNext()) {
                IDSorter idCountPair = iterator.next();
                topicSum += idCountPair.getWeight();
                for (int i = 0; i < weights.length; ++i) {
                    if (dataAlphabet.lookupObject(idCountPair.getID()).toString().equalsIgnoreCase(terms[i]))
                        weights[i] = idCountPair.getWeight();
                }
            }
            if (topicSum == 0) topicSum = 1;
            
            for (int i = 0; i < weights.length; ++i) {
                weights[i] = weights[i] / topicSum;
            }
            
            double pro = 1;
            for (int i = 0; i < terms.length; ++i) {
                if (weights[i] > 0 && !terms[i].equalsIgnoreCase(" ")) {            //TODO: smoothing needed
                    pro = pro * weights[i];
                } else {
                    pro = pro * (1 / topicSum);
                }
            }              
            return pro * numOfOrders(terms.length);
        }
        return 0;
    }
    

    /**
     *
     * @param term
     * @param topicID
     * @return P(w|t)
     */
    public double pwt(String term, int topicID) {
        
        int topicSum = 0;
        double weight = 0;
        Iterator<IDSorter> iterator = topicSortedWords.get(topicID).iterator();
        while (iterator.hasNext()) {
             IDSorter idCountPair = iterator.next();
             topicSum += idCountPair.getWeight();
             if (dataAlphabet.lookupObject(idCountPair.getID()).toString().equalsIgnoreCase(term))
                weight = idCountPair.getWeight();           
        }
        
        weight = weight / topicSum;            
        if (weight == 0) weight = 1/(double)topicSum;
        return weight;
    }
    
    

    /**
     *
     * @param topicID
     * @param docno
     * @return P(t|D)
     */
    public double ptd(int topicID, String docno) {
        int documentID = docnoInstanceIDMap.get(docno);
        double[] topicDistribution = model.getTopicProbabilities(documentID);
        return topicDistribution[topicID];
    }
    

    /**
     *
     * @param q
     * @param docno
     * @return P(q|D) q has to be under stemming
     * @throws IOException
     */
    public double pqd(String q, String docno) throws IOException {
        //int documentID = docnoInstanceIDMap.get(docno);
        String[] termList = docnoTextMap.get(docno).split(" ");
        String[] queryTermList = q.split(" ");
        double pro = 1;
        for (String queryTerm : queryTermList) {
            pro = pro * (tf(queryTerm, termList) / (double) termList.length);
        }
        return pro * numOfOrders(queryTermList.length);
    }
    
    
    /**
     *
     * @param term
     * @param docno
     * @return
     */
    public double pwd(String term, String docno) {
        String[] termList = docnoTextMap.get(docno).split(" ");
        double pro = tf(term, termList) / (double) termList.length;
        return pro;
    }
    
    
    /* return term frequency */
    private double tf(String term, String[] termlist) {
        int tf = 0;
        for (int i = 0; i < termlist.length; ++i) {
            if (termlist[i].equalsIgnoreCase(term)) tf++;
        }      
        return tf > 0 ? tf : 1;
    }
    
    /* don't use constants of multinorminal since query don't have same term */
    private int numOfOrders(int n) {
        if (n <= 0) return 1;
        int result = 1;
        for (int i = 1; i <= n; ++i) {
            result *= i;
        }
        return result;       
    }
    
    
    
    
    
    
        private class TopicID implements Comparable<TopicID> {
        private int topicID;
        private double pqt;
        
        public TopicID(int topicID, double pqt) {
            this.pqt = pqt;
            this.topicID = topicID;
        }
        
        public int getTopicID() {return topicID;}
        public double getPqt() {return pqt;}
        
        @Override
        public int compareTo(TopicID that) {
            if (this.pqt > that.pqt) return 1;
            if (this.pqt < that.pqt) return -1;
            return +0;
        }
    }
    
    private class extendWord implements Comparable<extendWord> {
        
        private String term;
        private double wq;
        
        public extendWord() {
            term = " ";
            wq = 0.0;
        }
        
        public extendWord(String term, double wq) {
            this.term = term;
            this.wq = wq;
        }
        
        public String getTerm() {
            return term;
        }
        
        public double getWQ() {
            return wq;
        }
        
        public int compareTo(extendWord that) {
            if (this.wq > that.wq) return 1;
            if (this.wq < that.wq) return -1;
            return +0;
        } 
    }
    
}
