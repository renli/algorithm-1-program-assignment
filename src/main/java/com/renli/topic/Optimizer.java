/*
 * To generate mapping structure instead of model object
 * 
 */
package com.renli.topic;

import cc.mallet.topics.ParallelTopicModel;
import cc.mallet.types.Alphabet;
import cc.mallet.types.IDSorter;
import cc.mallet.types.InstanceList;
import com.renli.util.Constants;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.TreeSet;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;

/**
 *
 * @author renli
 */
public class Optimizer {
    
    private ParallelTopicModel model;
    private InstanceList instances;
    private ArrayList<TreeSet<IDSorter>> topicSortedWords;
    private Alphabet dataAlphabet;
    private HashMap<String, Integer> docnoInstanceIDMap;
    private HashMap<String, String> docnoTextMap;
    private int numOfTopic;
    private IndexReader reader;
     
    public Optimizer(String model_url, String instance_url, String instanceHashmap_url, String textHashmap_url) throws FileNotFoundException, IOException, ClassNotFoundException {
        File modelSave = new File(model_url);
        File instanceSave = new File(instance_url);
        FileInputStream fis = new FileInputStream(modelSave);
        ObjectInputStream ois = new ObjectInputStream(fis);
        model = (ParallelTopicModel) ois.readObject();
        fis = new FileInputStream(instanceSave);
        ois = new ObjectInputStream(fis);
        instances = (InstanceList) ois.readObject();
        numOfTopic = model.getNumTopics();
        topicSortedWords = model.getSortedWords();
        dataAlphabet = instances.getDataAlphabet();
        File hashMap = new File(instanceHashmap_url);
        fis = new FileInputStream(hashMap);
        ois = new ObjectInputStream(fis);
        docnoInstanceIDMap = (HashMap<String, Integer>) ois.readObject();     
        Directory dir = FSDirectory.open(new File(Constants.INDEX_PATH));
	reader = DirectoryReader.open(dir);     
        hashMap = new File(textHashmap_url);
        fis = new FileInputStream(hashMap);
        ois = new ObjectInputStream(fis);
        docnoTextMap = (HashMap<String, String>) ois.readObject();
    }
    
    /**
     *
     * @param output_url
     * @throws IOException
     */
    public void generateTopicMap(String output_url) throws IOException {
        HashMap<Integer, HashMap<String, Double>> topic_distribution_map = new HashMap<Integer, HashMap<String, Double>>();
        
        for (int topicID = 0; topicID < numOfTopic; ++topicID) {
            Iterator<IDSorter> wordIterator = topicSortedWords.get(topicID).iterator();
            Iterator<IDSorter> sumIterator = topicSortedWords.get(topicID).iterator();
            HashMap<String, Double> word_distribution_map = new HashMap<String, Double>();
            /* compute sum */
            int sum = 0;
            while (sumIterator.hasNext()) {
                IDSorter idCountPair = sumIterator.next();
                sum += idCountPair.getWeight();
            }
            
            while (wordIterator.hasNext()) {
                IDSorter idCountPair = wordIterator.next();
                word_distribution_map.put(dataAlphabet.lookupObject(idCountPair.getID()).toString(), 
                        (double)idCountPair.getWeight()/sum);
            }
            
            topic_distribution_map.put(topicID, word_distribution_map);
                System.out.println("map topic: " + topicID);
        }
        
        File objSave = new File(output_url);
        FileOutputStream fos = new FileOutputStream(objSave);
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(topic_distribution_map);
    }
    
    
    /**
     *
     * @param output_url
     * @throws IOException
     */
    public void generateSumMap(String output_url) throws IOException {
        HashMap<Integer, Integer> topic_sum_map = new HashMap<Integer, Integer>();
        for (int topicID = 0; topicID < numOfTopic; ++topicID) {
            Iterator<IDSorter> sumIterator = topicSortedWords.get(topicID).iterator();
            int sum = 0;
            while (sumIterator.hasNext()) {
                IDSorter idCountPair = sumIterator.next();
                sum += idCountPair.getWeight();
            }
            topic_sum_map.put(topicID, sum);
        }
        File objSave = new File(output_url);
        FileOutputStream fos = new FileOutputStream(objSave);
        ObjectOutputStream oos = new ObjectOutputStream(fos);
        oos.writeObject(topic_sum_map);
    }
    
    public static void testSumMap(String input_url) throws FileNotFoundException, IOException, ClassNotFoundException {
        File modelSave = new File(input_url);
        FileInputStream fis = new FileInputStream(modelSave);
        ObjectInputStream ois = new ObjectInputStream(fis);
        HashMap<Integer, Integer> topic_sum_map = (HashMap<Integer, Integer>) ois.readObject();
        for (int i = 0; i < 200; ++i) {
            System.out.println(i + "\t" + topic_sum_map.get(i));
        }
        
    }

    public void testTopicMap(String input_url) throws FileNotFoundException, IOException, ClassNotFoundException {
        File modelSave = new File(input_url);
        FileInputStream fis = new FileInputStream(modelSave);
        ObjectInputStream ois = new ObjectInputStream(fis);
        HashMap<Integer, HashMap<String, Double>> map = (HashMap<Integer, HashMap<String, Double>>) ois.readObject();

        for (int i = 0; i < numOfTopic; ++i) {
            Iterator<IDSorter> wordIterator = topicSortedWords.get(i).iterator();
            while (wordIterator.hasNext()) {
                    IDSorter idCountPair = wordIterator.next();
                    System.out.println(dataAlphabet.lookupObject(idCountPair.getID()).toString() + ": " 
                        + map.get(i).get(dataAlphabet.lookupObject(idCountPair.getID()).toString()));
            }
        }            
    }
    
    public static void test(String input_url, String term) throws FileNotFoundException, IOException, ClassNotFoundException {
        File modelSave = new File(input_url);
        FileInputStream fis = new FileInputStream(modelSave);
        ObjectInputStream ois = new ObjectInputStream(fis);
        HashMap<Integer, HashMap<String, Double>> map = (HashMap<Integer, HashMap<String, Double>>) ois.readObject();   
        for (int i = 0; i < 200; ++i) {
            if (map.get(i).get(term) != null)
                System.out.println("topic " + i + "\t" + map.get(i).get(term) + "\t" + map.get(i).get(term));
        }
    }
    
}
