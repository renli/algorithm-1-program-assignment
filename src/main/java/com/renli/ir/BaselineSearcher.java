package com.renli.ir;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.document.Document;
import org.apache.lucene.search.Filter;
import org.apache.lucene.search.FieldCacheRangeFilter;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.FileWriter;
import java.lang.String;
import org.apache.lucene.analysis.en.EnglishAnalyzer;
import org.apache.lucene.search.similarities.BM25Similarity;
import org.apache.lucene.search.similarities.LMDirichletSimilarity;

/**
 *
 * @author renli
 */
public class BaselineSearcher {
    
    protected Query q;
    protected int querynum;
    //protected String searchtime;
        
    public BaselineSearcher(String content, int number) throws ParseException{
        //Analyzer analyzer = new StandardAnalyzer(Version.LUCENE_CURRENT);
        Analyzer analyzer = new EnglishAnalyzer(Version.LUCENE_CURRENT);
	QueryParser qp = new QueryParser(Version.LUCENE_CURRENT, "text", analyzer);
	q = qp.parse(content);
	querynum = number;
	//searchtime = docno;
    }
    
    public int Search(String indexURL, String outputURL) throws IOException{
		
	Directory dir = FSDirectory.open(new File(indexURL));
	IndexReader reader = DirectoryReader.open(dir);
        LMDirichletSimilarity simfm = new LMDirichletSimilarity(2500);   
        IndexSearcher searcher = new IndexSearcher(reader);
        searcher.setSimilarity(simfm);
	TopDocs results = searcher.search(q, 1000);	
	PrintWriter out = new PrintWriter(new FileWriter(outputURL, true));
            System.out.println("Basline search: " + q.toString());
            
	/* baseline task */	
	  for(int i=0; i<Math.min(1000, results.totalHits); i++){	// udpated from: Math.min(1000, results.totalHits)
		float score = results.scoreDocs[i].score;
		Document doc = reader.document(results.scoreDocs[i].doc);
                String docno = null;
                if (doc.getField("docno") != null)
                    docno = doc.getField("docno").stringValue();			// lower case		
                out.println(querynum + " Q0 " + docno +" " + i +" "+ score + " lren");
	 }
		
		out.close();
		return Math.min(1000, results.totalHits);
	}	
}
