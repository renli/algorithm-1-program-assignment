package com.renli.ir;

import cc.mallet.topics.LDAStream;
import com.renli.topic.CorpusGenerator;
import com.renli.topic.Optimizer;
import com.renli.topic.TopicModelTest;
import com.renli.topic.TopicModelUser;
import com.renli.util.Constants;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;

public class Run 
{
    public static void main( String[] args ) throws IOException, ParseException, FileNotFoundException, ClassNotFoundException
    {

        
        //Indexer index = new Indexer(Constants.CORPUS_PATH, "/run/media/renli/Projects/TopicProject/index_english/");   
        
        /* baseline and QE 
         * topic QE
         */
        
        BaselineSearcher baseline;
        QueryExpension qe;
        QueryReader query = new QueryReader(Constants.QUERY_PATH + "/queries.trec1.51-100");
        // tmu for topic QE
        //TopicModelUser tmu = new TopicModelUser(Constants.DATA_PATH + "/wsj_stemming_1000.model.obj", 
        //        Constants.DATA_PATH + "/wsj_stemming_1000.instance.obj", Constants.DATA_PATH + "/docno_instance.hashmap.obj",
        //        Constants.DATA_PATH + "/docno_text.hashmap.obj", Constants.DATA_PATH + "/topic_distribution.hashmap.obj",
        //        Constants.DATA_PATH + "/topic_sum.hashmap.obj");
        int count = 0;
        for (Query q : query.query) {
            count++;
            System.out.println("Searching: Q" + count + "\t" + q.getContent());       
            //baseline = new BaselineSearcher(q.getContent(), q.getNumber());
            //baseline.Search(Constants.INDEX_PATH, Constants.OUTPUT_PATH + "/WSJ.51-100.baseline.top");
            qe = new QueryExpension(q.getContent(), q.getNumber());       
            qe.SearchLDARM(Constants.INDEX_PATH, Constants.OUTPUT_PATH + "/WSJ.51-100.topicqe_test.top");   
        }
        
        
        
        //CorpusGenerator cg = new CorpusGenerator(Constants.CORPUS_PATH, Constants.OUTPUT_PATH + "/output");
        //cg.wsjGenerate();
        
        /* train topic model */
       //TopicModelTest.applyLDA(Constants.CORPUS_PATH + "/wsj_stemming.topic.corpus" ,Constants.OUTPUT_PATH + "/wsj_stemming_1000.model.obj", 
               //Constants.OUTPUT_PATH + "/wsj_stemming_1000.instance.obj", 200, 1000);
        //TopicModelTest.loadTest(Constants.DATA_PATH + "/wsj_stemming_1000.model.obj", Constants.DATA_PATH + "/wsj_stemming_1000.instance.obj",5);
        
        /*
        ArrayList<String> docnos = new ArrayList<String>();
        docnos.add("WSJ870324-0001");
        docnos.add("WSJ870323-0181");
        docnos.add("WSJ870323-0180");
        docnos.add("WSJ870323-0179");
        docnos.add("WSJ870323-0178");
        docnos.add("WSJ870323-0177");
        docnos.add("WSJ870323-0176");
        docnos.add("WSJ870323-0175");
        docnos.add("WSJ870323-0174");
        docnos.add("WSJ870323-0173");
        */
        
        /*
        TopicModelUser tmu = new TopicModelUser(Constants.DATA_PATH + "/wsj_stemming_1000.model.obj", 
                Constants.DATA_PATH + "/wsj_stemming_1000.instance.obj", Constants.DATA_PATH + "/docno_instance.hashmap.obj",
                Constants.DATA_PATH + "/docno_text.hashmap.obj", Constants.DATA_PATH + "/topic_distribution_sum.hashmap.obj");
        double test1 = tmu.pwq_sample("student",  "comput softwar", docnos);
        double test2 = tmu.pwq_sample("ibm",  "comput softwar", docnos);
        System.out.println("pwq student: " + test1);
        System.out.println("pwq ibm: " + test2);
        */
        /*
        TopicModelUser tmu = new TopicModelUser(Constants.DATA_PATH + "/wsj_stemming_1000.model.obj", 
                Constants.DATA_PATH + "/wsj_stemming_1000.instance.obj", Constants.DATA_PATH + "/docno_instance.hashmap.obj",
                Constants.DATA_PATH + "/docno_text.hashmap.obj", Constants.DATA_PATH + "/topic_distribution.hashmap.obj",
                Constants.DATA_PATH + "/topic_sum.hashmap.obj");
        ArrayList<String> out = tmu.ldaRMsample("comput softwar", docnos);
        for (String s : out) {
            System.out.println(s);
        }
        */
        
        /*
        Optimizer opt = new Optimizer(Constants.DATA_PATH + "/wsj_stemming_1000.model.obj", 
                Constants.DATA_PATH + "/wsj_stemming_1000.instance.obj", Constants.DATA_PATH + "/docno_instance.hashmap.obj",
                Constants.DATA_PATH + "/docno_text.hashmap.obj");
        opt.generateSumMap(Constants.DATA_PATH + "/topic_sum.hashmap.obj");
        //opt.testTopicMap(Constants.DATA_PATH + "/topic_distribution.hashmap.obj");
        * */

        //Optimizer.testSumMap(Constants.DATA_PATH + "/topic_sum.hashmap.obj");
        
        //TopicModelTest.addInstanceTest();

        
    }
}
