
package com.renli.ir;

/**
 *
 * @author renli
 */
/*
 * Read Query from topic file
 * */

import java.io.FileReader;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;

public class QueryReader {                  
	public ArrayList<Query> query;		
	public QueryReader(String FileName) throws IOException{
		
		query = new ArrayList<Query>();
		
		BufferedReader file = new BufferedReader(new FileReader(FileName)); 	
		while(file.ready()){
                    String line = file.readLine(); 
                    String[] spline = line.split("\\t");
                    
                    /*remove uncommon char */
                    StringBuilder contentString = new StringBuilder();
                    char[] chars = spline[1].toCharArray();
                    for(char c : chars){
                        if((c >= 65 && c <= 90) || (c >= 97 && c <= 122) || (c >= 48 && c <= 57) || c == ' '){
                            contentString.append(c);
                        }
                    }
                    contentString.append(" ");
                    
                    query.add(new Query(Integer.parseInt(spline[0]), contentString.toString()));
		}
		file.close();
	}
        
}
