
package com.renli.ir;

/**
 *
 * @author renli
 */
import static com.renli.ir.TrecWSJIteratorFirst.TYPE_STORED;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.Field.TermVector;
import org.apache.lucene.document.FieldType;
import org.apache.lucene.document.StringField;

public class TrecWSJIteratorSecond implements Iterator<Document> {
    protected BufferedReader rdr;
    protected boolean at_eof = false;
    
    public TrecWSJIteratorSecond(File file) throws FileNotFoundException {
        rdr = new BufferedReader(new FileReader(file));
        System.out.println("Reading " + file.toString());
    }
    
    public static final FieldType TYPE_STORED = new FieldType();

    static {
        TYPE_STORED.setIndexed(true);
        TYPE_STORED.setTokenized(true);
        TYPE_STORED.setStored(true);
        TYPE_STORED.setStoreTermVectors(true);
        TYPE_STORED.setStoreTermVectorPositions(true);
        TYPE_STORED.setStoreTermVectorOffsets(true);
        TYPE_STORED.setStoreTermVectorPayloads(true);
        TYPE_STORED.freeze();
    }
    
    @Override
        public boolean hasNext() {
        return !at_eof;
    }

    @Override
        public Document next() {
        Document doc = new Document();
        StringBuffer sb_text = new StringBuffer();
        StringBuffer sb_title = new StringBuffer();

        try {
            String line;


            boolean in_doc = false;
            boolean in_text = false;
            boolean in_lp = false;
            boolean in_docno = false;
            boolean in_title = false;
            
            while (true) {
                line = rdr.readLine();
                if (line == null) {
                    at_eof = true;
                    break;
                }

                if (in_doc) {
                    if (line.startsWith("<DOC>"))
                        doc = new Document();                        
                }

                if (!in_doc) {
                    if (line.startsWith("<DOC>"))
                        in_doc = true;
                    else
                        continue;
                }

                if (line.startsWith("</DOC>")) {
                    in_doc = false;
                    break;
                }
                
                if (in_docno && !line.startsWith("</DOCNO>")) {
                    doc.add(new StringField("docno", line, Field.Store.YES));
                }
                
                if ((in_text || in_lp) && !(line.startsWith("</LP>") || line.startsWith("</TEXT>"))) 
                    sb_text.append(line);
                
                if (in_title && !(line.contains("----") || line.contains("</HL>"))) {
                    sb_title.append(line);
                }
                
                /* conditions */
                if (in_doc) {
                    if (line.startsWith("<TEXT>")) {
                        in_text = true;
                    }
                    if (line.startsWith("<LP>")) {
                        in_lp = true;
                    }
                    if (line.startsWith("<DOCNO>")) {
                        in_docno = true;
                    }
                    if (line.startsWith("<HL>")) {
                        in_title = true;
                    }
                }
                               
                
                if (line.startsWith("</DOCNO>")) {
                    in_docno = false;
                }
                
                if (line.startsWith("</LP>")) {
                    in_lp = false;
                }
                
                if (line.startsWith("</TEXT>")) {
                   in_text = false;
                }
                
                if (line.startsWith("</HL>")) {
                    in_title = false;
                }
                
                if (in_title && line.contains("----")) {
                    in_title = false;
                }
                

            }
            if (sb_text.length() > 0)
                doc.add(new Field("text", sb_text.toString(),TYPE_STORED));
            if (sb_title.length() > 0)
                doc.add(new StringField("title", sb_title.toString(), Field.Store.YES));
            
        } catch (IOException e) {
            System.out.println(e);
            doc = null;
        }
        return doc;
    }

    @Override
        public void remove() {
        // Do nothing, but don't complain
    }
}
