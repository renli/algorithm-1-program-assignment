/*****************************************************************
 * ClassName: Indexer
 * 
 * Read corpus by String URL parameter corpus_url and output index
 * to target_url
 *****************************************************************/
package com.renli.ir;

/**
 *
 * @author renli
 */
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;

import java.io.File;
import java.io.IOException;
import java.util.Date;
import org.apache.lucene.analysis.en.EnglishAnalyzer;

public class Indexer {
	
	public Indexer(String corpus_url, String target_url) throws IOException, ParseException {
		
		// create index
		//Analyzer analyzer = new StandardAnalyzer(Version.LUCENE_CURRENT);       // using stemming ?
            
                Analyzer analyzer = new EnglishAnalyzer(Version.LUCENE_CURRENT);
		IndexWriterConfig conf = new IndexWriterConfig(Version.LUCENE_CURRENT, analyzer);
		conf.setOpenMode(IndexWriterConfig.OpenMode.CREATE);
		Directory dir = FSDirectory.open(new File(target_url));
		IndexWriter index = new IndexWriter(dir, conf);
		
		// read filename
		File file=new File(corpus_url);
		File[] files = file.listFiles();
		if (files == null){
                        System.out.println("No input file");
			return; 
		}
		
		//timer
		long startTime = new Date().getTime();
		Document document;
		
		for (int i = 0; i<files.length; ++i) {
                        if (files[i].getName().contains("87") || files[i].getName().contains("88") 
                                || files[i].getName().contains("89")) {
                            TrecWSJIteratorFirst docs = new TrecWSJIteratorFirst(files[i]);         //TODO: refactor needed
                            
                            System.out.println("Construct First Iterator");
                                
                            while (docs.hasNext()) {
				document = docs.next();
                                if (document.getField("docno") != null)
                                    index.addDocument(document);
                            }
                        } else {
                            TrecWSJIteratorSecond docs = new TrecWSJIteratorSecond(files[i]);
                            
                            System.out.println("Construct Second Iterator");
                                
                            while (docs.hasNext()) {
				document = docs.next();
                                if (document.getField("docno") != null)
                                    index.addDocument(document);
                            }
                        }     
		}
		
		System.out.println(index.numDocs());
		index.close();	
		long endTime = new Date().getTime();
		System.out.println(endTime - startTime + "ms");	
	}	
}
