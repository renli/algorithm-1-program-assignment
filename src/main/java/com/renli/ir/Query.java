/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.renli.ir;

/**
 *
 * @author renli
 */
public class Query {
    private int querynumber;
    private String query;
            
    public Query(int qn, String q) {
        querynumber = qn;
        query = q;
    }
            
    public int getNumber() {
        return querynumber;
    }
            
    public String getContent() {
        return query;
    }
}
