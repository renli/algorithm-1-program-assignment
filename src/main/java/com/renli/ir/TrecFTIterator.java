package com.renli.ir;

import static com.renli.ir.TrecWSJIteratorFirst.TYPE_STORED;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.FieldType;
import org.apache.lucene.document.StringField;

/**
 *
 * @author renli
 */
public class TrecFTIterator implements Iterator<Document>{
    protected BufferedReader rdr;
    protected boolean at_eof = false;
    
    public TrecFTIterator(File file) throws FileNotFoundException {
        rdr = new BufferedReader(new FileReader(file));
        System.out.println("Reading " + file.toString());
    }
    
    public static final FieldType TYPE_STORED = new FieldType();
    
    @Override
    public boolean hasNext() {
         return !at_eof;
    }
    
    @Override
    public Document next() {
        Document doc = new Document();
        StringBuffer sb_text = new StringBuffer();
        StringBuffer sb_title = new StringBuffer();
        try{
            String line;
            Pattern docno_tag = Pattern.compile("<DOCNO>\\s*(\\S+)\\s*<");
            boolean in_doc = false;
            boolean in_text = false;
            boolean in_title = false;           
            while (true) {
                line = rdr.readLine();
                if (line == null) {
                    at_eof = true;
                    break;
                }
                if (in_doc) {
                    if (line.startsWith("<DOC>"))
                        doc = new Document();                        
                }

                if (!in_doc) {
                    if (line.startsWith("<DOC>"))
                        in_doc = true;
                    else
                        continue;
                }

                if (line.startsWith("</DOC>")) {
                    in_doc = false;
                    break;
                }
                
                if (in_text && !line.startsWith("</TEXT>")) 
                    sb_text.append(line);
                
                if (in_doc) {
                    if (line.startsWith("<TEXT>")) {
                        in_text = true;
                    }
                }
                
                if (line.startsWith("</TEXT>")) {
                        in_text = false;
                }
                
                if (line.startsWith("<HEADLINE>")) {
                        in_title = true;            
                }
                
                if (line.indexOf("</HEADLINE>") > 0) {
                    //TODO
                }
                
                Matcher m = docno_tag.matcher(line);
                if (m.find()) {
                    String docno = m.group(1);
                    doc.add(new StringField("docno", docno, Field.Store.YES));
                }
                
            }
            
            
          if (sb_text.length() > 0)
                doc.add(new Field("text", sb_text.toString(),TYPE_STORED));  
          
        } catch (IOException e) {
            System.out.println(e);
            doc = null;
        }
        return doc;       
    }
    
    @Override
    public void remove() {
    }
    
}
