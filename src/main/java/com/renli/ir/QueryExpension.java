
package com.renli.ir;


import static com.renli.topic.CorpusGenerator.tokenizeString;
import com.renli.topic.LDARMUser;
import com.renli.topic.TopicModelUser;
import com.renli.util.Constants;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.en.EnglishAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.search.similarities.LMDirichletSimilarity;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.Version;

/**
 *
 * @author renli
 */
public class QueryExpension {
    protected Query q;
    protected int querynum;
    protected String content;
    protected ArrayList<String> extended_terms;
    
    public QueryExpension(String content, int number) throws ParseException {
        Analyzer analyzer = new EnglishAnalyzer(Version.LUCENE_CURRENT);
	QueryParser qp = new QueryParser(Version.LUCENE_CURRENT, "text", analyzer);
	q = qp.parse(content);
	querynum = number;
	this.content = content;
    }
    
    

    /**
     *
     * @param indexURL
     * @param outputURL
     * @param tmu
     * @return query expension run 
     * @throws IOException
     * @throws ParseException
     */
    public int Search(String indexURL, String outputURL, TopicModelUser tmu) throws IOException, ParseException {
        Directory dir = FSDirectory.open(new File(indexURL));
	IndexReader reader = DirectoryReader.open(dir);
        LMDirichletSimilarity simfm = new LMDirichletSimilarity(2500);  
        IndexSearcher searcher = new IndexSearcher(reader);
        searcher.setSimilarity(simfm);
	TopDocs results = searcher.search(q, 10);	
	PrintWriter out = new PrintWriter(new FileWriter(outputURL, true));
        extended_terms = new ArrayList<String>();
        
        /* first search */
        ArrayList<String> docnos = new ArrayList<String>();         // extend documents
        for(int i = 0; i < Math.min(10, results.totalHits); ++i){
            Document doc = reader.document(results.scoreDocs[i].doc);   
            docnos.add(doc.get("docno"));   // add docno to list for topic qe
        }

        /* remove stop words */
        ArrayList<String> terms = new ArrayList();
        String[] query_terms = removeStopword(content.split(" "));
        terms.addAll(Arrays.asList(query_terms));
        StringBuilder common_text = new StringBuilder();
        for(String t : terms){
            t = t.replaceAll("(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]", "");
            /* remove uncommon char and number */
            char[] chars = t.toCharArray();
            for(char c : chars){
                if((c >= 65 && c <= 90) || (c >= 97 && c <= 122) || (c >= 48 && c <= 57)){
                    common_text.append(c);
                }
            }
            common_text.append(" ");
        }    
        
        /* stemming query */            
        StringBuilder text = new StringBuilder();
        EnglishAnalyzer sa = new EnglishAnalyzer(Version.LUCENE_CURRENT);
        ArrayList<String> tokenlist = tokenizeString(sa, common_text.toString());
        for (String sample : tokenlist) {
            text.append(sample);
            text.append(" ");
        }        
        /* add extended terms */
        extended_terms = tmu.ldaRMsample(text.toString(), docnos);
        
        
        /* 2nd search */
        StringBuilder query = new StringBuilder();
        //Query qe;
        Analyzer analyzer = new EnglishAnalyzer(Version.LUCENE_CURRENT);
	QueryParser qp = new QueryParser(Version.LUCENE_CURRENT, "text", analyzer);
        
        for (String t : extended_terms) {
            query.append(t);
            query.append(" ");
        }
        query.append(content);      
        
	q = qp.parse(query.toString());                 System.out.println("search: " + q.toString());
        results = searcher.search(q,  1000);
        
        /* output task */	
	  for(int i=0; i<Math.min(1000, results.totalHits); i++){	
		float score = results.scoreDocs[i].score;
		Document doc = reader.document(results.scoreDocs[i].doc);
		String docno = doc.getField("docno").stringValue();			// lower case		
			out.println(querynum + " Q0 " + docno +" " + i +" "+ score + " wsj_tpqe");
	 }
         out.flush();
         out.close();
	 return Math.min(1000, results.totalHits);
    }
    
    
    public int SearchLDARM(String indexURL, String outputURL) throws IOException, ParseException, FileNotFoundException, ClassNotFoundException {
        Directory dir = FSDirectory.open(new File(indexURL));
	IndexReader reader = DirectoryReader.open(dir);
        LMDirichletSimilarity simfm = new LMDirichletSimilarity(2500);  
        IndexSearcher searcher = new IndexSearcher(reader);
        searcher.setSimilarity(simfm);
	TopDocs results = searcher.search(q, 50);	
	PrintWriter out = new PrintWriter(new FileWriter(outputURL, true));
        extended_terms = new ArrayList<String>();
        
        /* first search */
        ArrayList<String> docnos = new ArrayList<String>();         // extend documents
        for(int i = 0; i < Math.min(50, results.totalHits); ++i){
            Document doc = reader.document(results.scoreDocs[i].doc);   
            docnos.add(doc.get("docno"));   // add docno to list for topic qe
        }

        /* remove stop words */
        ArrayList<String> terms = new ArrayList();
        String[] query_terms = removeStopword(content.split(" "));
        terms.addAll(Arrays.asList(query_terms));
        StringBuilder common_text = new StringBuilder();
        for(String t : terms){
            t = t.replaceAll("(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]", "");
            /* remove uncommon char and number */
            char[] chars = t.toCharArray();
            for(char c : chars){
                if((c >= 65 && c <= 90) || (c >= 97 && c <= 122) || (c >= 48 && c <= 57)){
                    common_text.append(c);
                }
            }
            common_text.append(" ");
        }    
        
        /* stemming query */            
        StringBuilder text = new StringBuilder();
        EnglishAnalyzer sa = new EnglishAnalyzer(Version.LUCENE_CURRENT);
        ArrayList<String> tokenlist = tokenizeString(sa, common_text.toString());
        for (String sample : tokenlist) {
            text.append(sample);
            text.append(" ");
        }        
        /* add extended terms */
        LDARMUser ldausr = new LDARMUser(Constants.DATA_PATH + "/docno_text.hashmap.obj", docnos, 100, 1000);
        extended_terms = ldausr.ldaRMsample(text.toString());
        
        
        /* 2nd search */
        StringBuilder query = new StringBuilder();
        //Query qe;
        Analyzer analyzer = new EnglishAnalyzer(Version.LUCENE_CURRENT);
	QueryParser qp = new QueryParser(Version.LUCENE_CURRENT, "text", analyzer);
        
        for (String t : extended_terms) {
            query.append(t);
            query.append(" ");
        }
        query.append(content);      
        
	q = qp.parse(query.toString());                 System.out.println("search: " + q.toString());
        results = searcher.search(q,  1000);
        
        /* output task */	
	  for(int i=0; i<Math.min(1000, results.totalHits); i++){	
		float score = results.scoreDocs[i].score;
		Document doc = reader.document(results.scoreDocs[i].doc);
		String docno = doc.getField("docno").stringValue();			// lower case		
			out.println(querynum + " Q0 " + docno +" " + i +" "+ score + " wsj_tpqe");
	 }
         out.flush();
         out.close();
	 return Math.min(1000, results.totalHits);
    }
    
    
    public static String[] removeStopword(String[] terms) throws FileNotFoundException, IOException{
        
        ArrayList<String> result = new ArrayList();
        ArrayList<String> stopwords = new ArrayList();
        File words = new File("src/main/resources/stop_list");
        BufferedReader bf = new BufferedReader(new FileReader(words));
        if(terms != null){
            while(bf.ready()){
                stopwords.add(bf.readLine());
            }
            
            for(int i=0; i<terms.length; ++i){
                boolean flag = true;
                for(int j=0; j<stopwords.size(); ++j)
                    if(terms[i].equals(stopwords.get(j))){
                        flag = false;
                } 
                if(flag) result.add(terms[i]);
            }   
            return result.toArray(new String[result.size()]);
            }
        return null;
    }
    
}
