
package com.renli.util;

/**
 *
 * @author renli
 */
public final class Constants {
    public static final String CORPUS_PATH = "/run/media/renli/Projects/TopicProject/corpus/WSJ"; 
    public static final String QUERY_PATH = "/run/media/renli/Projects/TopicProject/topics";
    public static final String OUTPUT_PATH = "/run/media/renli/Projects/TopicProject/output";
    public static final String INDEX_PATH = "/run/media/renli/Projects/TopicProject/index_english";
    public static final String DATA_PATH = "/run/media/renli/Projects/TopicProject/data";
    //public static final String DATABASE_URL = "jdbc:mysql://localhost:3306/microblock";
}
