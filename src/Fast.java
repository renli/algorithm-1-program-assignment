
import java.util.Arrays;


/**
 *
 * @author renli
 */
public class Fast {
    public static void main(String[] args) {
        // reset
        StdDraw.setXscale(0, 32768);
        StdDraw.setYscale(0, 32768);
        Point point[];
        Point Reference[];
        if (args.length > 0) {
            String argFileName = args[0];
           try {
                In in = new In(argFileName);
                int N = in.readInt();
                point = new Point[N];
                Reference = new Point[N];
                int index = 0;
                while (!in.isEmpty()) {
                    int x = in.readInt();
                    int y = in.readInt();
                    point[index] = new Point(x, y);
                    Reference[index] = new Point(x, y);
                    point[index].draw();
                    index++;
                }
                
                //Fasth
                Point lines[][] = new Point[N][2];
                int line_index = 0;
                for (int h = 0; h < N; ++h) {
                    Arrays.sort(point, Reference[h].SLOPE_ORDER);
                    // search
                    int i = 0;
                    int j = 1;
                    while (j <= N) {
                        if (Reference[h].slopeTo(point[i]) == Reference[h].slopeTo(point[j])) {
                            j++;
                            // last rote
                            if (j - i > 2 && j > N) {
                                Point subarray[] = new Point[j - i + 1];
                                for (int l = 0; l < j - i; ++l ) {
                                    subarray[l] = point[i + l];
                                }
                                subarray[j - i] = Reference[h];
                                Merge.sort(subarray);
                                // print & draw
                                boolean drawflag = true;
                                for (int li = 0; li < N; ++li) {
                                    if (lines[li][0] != null) {
                                        if (lines[li][0].compareTo(subarray[0]) == 0 && lines[li][1].compareTo(subarray[1]) == 0)
                                            drawflag = false;
                                    }
                                }
                                
                                if (drawflag == true) {
                                    lines[line_index][0] = subarray[0];
                                    lines[line_index][1] = subarray[1];
                                    line_index++;
                                }
                                    
                                if (drawflag == true) {
                                    for (int k = 0; k < j - i; ++k) {
                                        StdOut.print(subarray[k].toString() + " -> ");
                                        subarray[k].drawTo(subarray[k + 1]);
                                    }
                                    StdOut.print(subarray[j - i].toString() + "\n");
                                } 
                               
                            }
                        } else {
                            if (j - i > 2) {    
                               Point subarray[] = new Point[j - i + 1];
                                for (int l = 0; l < j - i; ++l ) {
                                    subarray[l] = point[i + l];
                                }
                                subarray[j - i] = Reference[h];
                                Merge.sort(subarray);
                                // print & draw
                                boolean drawflag = true;
                                for (int li = 0; li < N; ++li) {
                                    if (lines[li][0] != null) {
                                        if (lines[li][0].compareTo(subarray[0]) == 0 && lines[li][1].compareTo(subarray[1]) == 0)
                                            drawflag = false;
                                    }
                                }
                                
                                if (drawflag == true) {
                                    lines[line_index][0] = subarray[0];
                                    lines[line_index][1] = subarray[1];
                                    line_index++;
                                }
                                    
                                if (drawflag == true) {
                                    for (int k = 0; k < j - i; ++k) {
                                        StdOut.print(subarray[k].toString() + " -> ");
                                        subarray[k].drawTo(subarray[k + 1]);
                                    }
                                    StdOut.print(subarray[j - i].toString() + "\n");
                                } 
                             
                            } else {
                                j++;
                                i = j - 1;
                            }
                        }
                    }
                    
                }
                
                
            } catch(Exception ex) {
                StdOut.println(ex.toString());
            }
        } else {
            StdOut.println("main() - No arguments.");
        }
    }
}
