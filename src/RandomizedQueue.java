
import java.util.Iterator;


/**
 *
 * @author renli
 */
public class RandomizedQueue<Item> implements Iterable<Item> {
    
   private int size;
   private Item[] items;
   
   
   public RandomizedQueue() {            // construct an empty randomized queue
       size = 0;
       items = (Item[]) new Object[1];  // initialize size to 1
   }
   
   public boolean isEmpty() {            // is the queue empty?
       return size == 0;
   }
           
   public int size() {                   // return the number of items on the queue
       return size;
   }                  
   
   public void enqueue(Item item) {      // add the item
       if (item != null) {
            if (size == items.length) 
                resize(items.length*2);
            items[size++] = item;       // notation: not ++size !
       } else {
           throw new java.lang.NullPointerException();
       }
   }
   
   private void resize(int capacity) {               // resize factor = 2
       Item[] copy = (Item[]) new Object[capacity];
       for (int i = 0; i < size; ++i) 
           copy[i] = items[i];
       items = copy;
   }
   
   public Item dequeue() {               // delete and return a random item
       if (!isEmpty()) {
            int index = StdRandom.uniform(0, size);
            Item result = items[index];
            //for (int i = index; i < size - 1; ++i) {
            //     items[i] = items[i+1];
            //}
            items[index] = items[size - 1];
            items[size - 1] = null;     // release last element
            size--;
            if (size == items.length/4 && size > 0)  // resize trigger factor = 4
                resize(items.length/2);               // bug: size should never reach 0 !
            return result;
            
       } else {
           throw new java.util.NoSuchElementException();
       }
   }
   
   public Item sample() {               // return (but do not delete) a random item
       if (!isEmpty()) {
            int index = StdRandom.uniform(0, size);
            return items[index];
       } else {
           throw new java.util.NoSuchElementException();
       }
   }
   
   // return an independent iterator over items in random order
   public Iterator<Item> iterator() { return new ArrayIterator(); }
   
   private class ArrayIterator implements Iterator<Item> {
       
        private boolean order = StdRandom.bernoulli();;      
        private int i = size;
        
        ArrayIterator(){
            order = StdRandom.bernoulli();
            if (order) {
                i = size;
            } else {
                i = 0;
            }
        }
        
        @Override
        public boolean hasNext() { return i > 0; }
        
        @Override
        public void remove() { 
            throw new java.lang.UnsupportedOperationException(); 
        }
        
        @Override
        public Item next() {
            if (order) {
                if (i > 0) {
                    return items[--i]; 
                } else {
                    throw new java.util.NoSuchElementException();
                }
            } else {
                if (i < size) {
                    return items[i++];
                } else {
                    throw new java.util.NoSuchElementException();
                }
            }
        }
        
   }
}
