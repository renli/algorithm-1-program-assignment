


/**
 *
 * @author renli
 */
public class Board {
    private int N;                          //dimension
    private int[][] board;
    public Board(int[][] blocks) {          // construct a board from an N-by-N array of blocks
        if (blocks != null) {
            N = blocks.length;
        } else {
            N = 0;
        }
        
        board = new int[N][N];
        for (int i = 0; i < N; ++i) {
            for (int j = 0; j < N; ++j) {
                board[i][j] = blocks[i][j];
            }
        }
    }          
                                           // (where blocks[i][j] = block in row i, column j)
    public int dimension() {
        return N;
    }                // board dimension N
    
    public int hamming() {
        int order = 0;
        for (int i = 0; i < N; ++i) {
            for (int j = 0; j < N; ++j) {
                if (board[i][j] != i * N + j + 1) order++;
            }
        }
        return order - 1;         // return the number except the last 0
    }                   // number of blocks out of place
    
    
    public int manhattan() {
        int mh = 0;
        for (int i = 0; i < N; ++i) {
            for (int j = 0; j < N; ++j) {
                if (board[i][j] != 0 && (board[i][j] != i * N + j + 1)) {
                    int r = (board[i][j] - 1) / N;
                    int c = board[i][j] - r * N - 1;
                    mh += Math.abs(r - i) + Math.abs(c - j);
                }
            }
        }
        return mh;
    }                // sum of Manhattan distances between blocks and goal
    
    
    public boolean isGoal() {
        for (int i = 0; i < N; ++i) {
            for (int j = 0; j < N; ++j) {
                if (board[i][j] > 0 && (board[i][j] != i * N + j + 1)) return false;
            }
        }
        return true;
    }               // is this board the goal board?
    
    
    public Board twin() {       
        if (board != null) {
            int[][] blocks = new int[N][N];
            for (int i = 0; i < N; ++i) {
                for (int j = 0; j < N; ++j) {
                    blocks[i][j] = board[i][j];
                }
            }
            if (blocks[0][N - 1] != 0 && blocks[0][N - 2] != 0) {
                blocks[0][N - 1] = blocks[0][N - 1] + blocks[0][N - 2];      //b = a + b
                blocks[0][N - 2] = blocks[0][N - 1] - blocks[0][N - 2];      //a = b - a
                blocks[0][N - 1] = blocks[0][N - 1] - blocks[0][N - 2];      //b = b - a
            } else {
                blocks[1][N - 1] = blocks[1][N - 1] + blocks[1][N - 2];      //b = a + b
                blocks[1][N - 2] = blocks[1][N - 1] - blocks[1][N - 2];      //a = b - a
                blocks[1][N - 1] = blocks[1][N - 1] - blocks[1][N - 2];      //b = b - a
            }
            
            return new Board(blocks);
        }
       return null; 
    }                   // a board obtained by exchanging two adjacent blocks in the same row
    
    @Override
    public boolean equals(Object y) {
        if (y == this) return true;
        if (y == null) return false;
        if (y.getClass() != this.getClass()) return false;
        Board that = (Board) y;
        if (this.dimension() != that.dimension()) return false;
        for (int i = 0; i < N; ++i) {
            for (int j = 0; j < N; ++j) {              
                if (this.board[i][j] != that.board[i][j]) return false;
            }
        }
        return true;
    }       // does this board equal y?

    
    public Iterable<Board> neighbors() {
        Queue result = new Queue();
        for (int i = 0; i < N; ++i) {
            for (int j = 0; j < N; ++j) {
                if (board[i][j] == 0) {
                    if (j - 1 >= 0) {
                        board[i][j] = board[i][j - 1];
                        board[i][j - 1] = 0;
                        result.enqueue(new Board(board));
                        board[i][j - 1] = board[i][j];
                        board[i][j] = 0;
                    }
                    if (j + 1 < N) {
                        board[i][j] = board[i][j + 1];
                        board[i][j + 1] = 0;
                        result.enqueue(new Board(board));
                        board[i][j + 1] = board[i][j];
                        board[i][j] = 0;
                    }
                    if (i - 1 >= 0) {
                        board[i][j] = board[i - 1][j];
                        board[i - 1][j] = 0;
                        result.enqueue(new Board(board));
                        board[i - 1][j] = board[i][j];
                        board[i][j] = 0;
                    }
                    if (i + 1 < N) {
                        board[i][j] = board[i + 1][j];
                        board[i + 1][j] = 0;
                        result.enqueue(new Board(board));
                        board[i + 1][j] = board[i][j];
                        board[i][j] = 0;
                    }
                }
            }
        }
        return result;
    }    // all neighboring boards return a queue
            
    @Override
    public String toString() {              // string representation of the board (in the output format specified below)
    StringBuilder s = new StringBuilder();
    s.append(N).append("\n");
    for (int i = 0; i < N; i++) {
        for (int j = 0; j < N; j++) {
            s.append(String.format("%2d ", board[i][j]));
        }
        s.append("\n");
    }
    return s.toString();
    }                
            
}
