



/**
 *
 * @author renli
 */
public class Brute {    
    
    public static void main(String[] args) {
        // reset
        StdDraw.setXscale(0, 32768);
        StdDraw.setYscale(0, 32768);
        Point point[];
        if (args.length > 0) {
            String argFileName = args[0];
            try {
                In in = new In(argFileName);
                int N = in.readInt();
                point = new Point[N];
                int index = 0;
                while (!in.isEmpty()) {
                    int x = in.readInt();
                    int y = in.readInt();
                    point[index] = new Point(x, y);
                    point[index].draw();
                    index++;
                }
                
                //Brute
                for (int p = 0; p < N; ++p)
                    for (int q = p + 1; q < N; ++q)
                        for (int r = q + 1; r < N; ++r)
                            for (int s = r + 1; s < N; ++s) {
                                if (point[p].slopeTo(point[q]) == point[p].slopeTo(point[r])
                                        && point[p].slopeTo(point[q]) == point[p].slopeTo(point[s])) {
                                    point[p].drawTo(point[q]);
                                    point[p].drawTo(point[r]);
                                    point[p].drawTo(point[s]);
                                    StdOut.println(point[p].toString() + " -> " + point[q].toString() + " -> " +
                                            point[r].toString() + " -> " + point[s].toString());
                                }
                            }
                
            } catch(Exception ex) {
                StdOut.println(ex.toString());
            }
        } else {
            StdOut.println("main() - No arguments.");
        }
    }
}
