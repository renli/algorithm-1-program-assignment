
import java.util.Comparator;



/**
 *
 * @author renli
 */
public class Solver {
    private boolean solvable;
    private Stack<Board> solution;
    private MinPQ init;
    private MinPQ twin;
    private int initSteps;
    private int twinSteps;
    private int moves;
    public Solver(Board initial) {
        init = new MinPQ(new CompareBoard());
        twin = new MinPQ(new CompareBoard());
        init.insert(new Node(initial, null, 0));
        twin.insert(new Node(initial.twin(), null, 0));
        Node initSol = (Node) init.min();
        Node twinSol = (Node) twin.min();
        solution = new Stack();
        initSteps = 0;
        twinSteps = 0;
        while (!initSol.getCurrent().isGoal() && !twinSol.getCurrent().isGoal()) {
            
            initSteps = initSol.moves + 1;
            twinSteps = twinSol.moves + 1;        
            for (Board initNeighbor : initSol.getCurrent().neighbors()) {
                if (initSol.getPrevious() != null) {
                    if (!initNeighbor.equals(initSol.getPrevious().getCurrent())) {
                        init.insert(new Node(initNeighbor, initSol, initSteps));
                    }
                } else {
                    init.insert(new Node(initNeighbor, initSol, initSteps));
                }
            }
            
             for (Board twinNeighbor : twinSol.getCurrent().neighbors()) {
                 if (twinSol.getPrevious() != null) {
                    if (!twinNeighbor.equals(twinSol.getPrevious().current)) {
                        twin.insert(new Node(twinNeighbor, twinSol, twinSteps));
                    }
                 } else {
                     twin.insert(new Node(twinNeighbor, twinSol, twinSteps));
                 }
            }
             
            initSol = (Node) init.delMin();
            twinSol = (Node) twin.delMin();
        }
        
        if (twinSol.getCurrent().isGoal()) {
            solvable = false;
            moves = -1;
        } else {
            solvable = true;
            moves = initSol.getMoves();
        }
        
        // add to solution
        solution.push(initSol.getCurrent());
        while (initSol.getPrevious() != null) {
            initSol = initSol.getPrevious();
            solution.push(initSol.getCurrent());
        }
    }           // find a solution to the initial board (using the A* algorithm)
    
    
    public boolean isSolvable() {
        return solvable;
    }            // is the initial board solvable?
    public int moves() {
        return moves;
    }                     // min number of moves to solve initial board; -1 if no solution
    
    public Iterable<Board> solution() {
        if (solvable) return solution;
        return null;
    }       // sequence of boards in a shortest solution; null if no solution
    
    private class CompareBoard implements Comparator<Node> {
        @Override
        public int compare(Node t1, Node t2) {
            if (t1.current.manhattan() + t1.moves > t2.current.manhattan() + t2.moves) return 1;
            if (t1.current.manhattan() + t1.moves < t2.current.manhattan() + t2.moves) return -1;
            return +0;
        }
    }
    
    private class Node {
        private Board current;
        private Node previous;
        private int moves;
        public Node(Board current, Node previous, int steps) {
            this.current = current;
            this.previous = previous;
            moves = steps;
        }
        public Board getCurrent() {
            return current;
        }
        
        public Node getPrevious() {
            return previous;
        }
        
        public int getMoves() {
            return moves;
        }
    }
            
    public static void main(String[] args) {
    // create initial board from file
        In in = new In(args[0]);
        int N = in.readInt();
        int[][] blocks = new int[N][N];
        for (int i = 0; i < N; i++)
            for (int j = 0; j < N; j++)
                blocks[i][j] = in.readInt();
        Board initial = new Board(blocks);

    // solve the puzzle
        Solver solver = new Solver(initial);

    // print solution to standard output
        if (!solver.isSolvable())
            StdOut.println("No solution possible");
        else {
            StdOut.println("Minimum number of moves = " + solver.moves());
        for (Board board : solver.solution())
            StdOut.println(board);
        }
    }
}
