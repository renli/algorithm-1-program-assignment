

/**
 *
 * @author renli
 */
public class PointSET {
   private SET<Point2D> set = new SET<Point2D>(); 
   public PointSET() {
       
   }                              // construct an empty set of points
   
   public boolean isEmpty() {
       return set.isEmpty();
   }                       // is the set empty?
   
   public int size() {
       return set.size();
   }                               // number of points in the set
   
   public void insert(Point2D p) {
       if (!contains(p)) set.add(p);
   }                  // add the point p to the set (if it is not already in the set)
   
   public boolean contains(Point2D p) {
       return set.contains(p);
   }             // does the set contain the point p?
   
   public void draw() {
       //Draw d = new Draw();
       for (Point2D p : set) {
           //d.point(p.x(), p.y());
           StdDraw.point(p.x(), p.y());
       }
   }                             // draw all of the points to standard draw
   
   public Iterable<Point2D> range(RectHV rect) {
       Queue<Point2D> result = new Queue();
       for (Point2D p : set) {
           if (rect.contains(p)) result.enqueue(p);
       }
       return result;
   }     // all points in the set that are inside the rectangle
   
   public Point2D nearest(Point2D p) {
       Point2D result = set.iterator().next();
       double minDistance = result.distanceTo(p);
       for (Point2D sp : set) {
           if (sp.distanceTo(p) < minDistance) {
               minDistance = sp.distanceTo(p);
               result = sp;
           }
       }
       return result;
   }               // a nearest neighbor in the set to p; null if set is empty
}
