
/**
 *
 * @author renli
 */
public class KdTree {
    private int size;
    private Node root;
    public KdTree() {
        size = 0;
        root = null;
    }
    
   public boolean isEmpty() {
       return size() == 0;
   }                       // is the set empty?
   public int size() {
       return size;
   } // number of points in the set
   
   public void insert(Point2D p) {
       if (!contains(p)) {
           root = put(root, p);
           size++;
       }      
   } // add the point p to the set (if it is not already in the set)
   
   public boolean contains(Point2D p) {
       Node x = root;
       while (x != null) {
           if (x.root().equals(p)) return true;
           if (x.xy()) {
               if (p.x() < x.root().x()) {
                   x = x.lb;
               } else {
                   x = x.rt;
               }
           } else {
               if (p.y() < x.root().y()) {
                   x = x.lb;
               } else {
                   x = x.rt;
               }
           }
       }
       return false;
   }             // does the set contain the point p?
   
   public void draw() {
       //Draw d = new Draw();
       drawNode(root);
   }            // draw all of the points to standard draw
   
   private void drawNode(Node n) {
       if (n == null) return;
       if (n.xy()) {
           StdDraw.setPenColor(StdDraw.BLACK);
           StdDraw.point(n.root().x(), n.root().y());
           StdDraw.setPenColor(StdDraw.RED);
           StdDraw.line(n.root().x(), n.getRect().ymin(), n.root().x(), n.getRect().ymax());
       } else {
           StdDraw.setPenColor(StdDraw.BLACK);
           StdDraw.point(n.root().x(), n.root().y());
           StdDraw.setPenColor(StdDraw.BLUE);
           StdDraw.line(n.getRect().xmin(), n.root().y(), n.getRect().xmax(), n.root().y());
       }
       if (n.leftBottom() != null)
           drawNode(n.leftBottom());
       if (n.rightTop() != null)
           drawNode(n.rightTop());
   }
   
   public Iterable<Point2D> range(RectHV rect) {
       Queue<Point2D> result = new Queue();
       search(rect, root, result);
       return result;
   }    // all points in the set that are inside the rectangle
   
   private void search(RectHV rect, Node r, Queue<Point2D> result) {
       if (r == null) return;
       if (rect.contains(r.root())) result.enqueue(r.root());
       if (r.leftBottom() != null)
            if (rect.intersects(r.leftBottom().getRect())) search(rect, r.leftBottom(), result);
       if (r.rightTop() != null)
            if (rect.intersects(r.rightTop().getRect())) search(rect, r.rightTop(), result);
   }
   
   public Point2D nearest(Point2D p) {
       Point2D result = null;
       if (root != null)
            result = searchNearest(root, p, root.root());
       return result;
   }              // a nearest neighbor in the set to p; null if set is empty
   
   private Point2D searchNearest(Node r, Point2D query, Point2D nearest) {
       Point2D near = nearest;
       if (r == null) return near;
       if (r.root().distanceSquaredTo(query) < nearest.distanceSquaredTo(query))
           near = r.root();
       
       if (r.getRect().contains(query)) {
       if (r.xy()) {         
           if (Math.abs(r.root().x() - query.x()) < nearest.distanceSquaredTo(query)) {
               if (r.leftBottom() != null)
                   near = searchNearest(r.leftBottom(), query, near);
               if (r.rightTop() != null)
                   near = searchNearest(r.rightTop(), query, near);
           } else {
               if (r.leftBottom() != null)
                   if (query.x() < r.root().x())
                       near = searchNearest(r.leftBottom(), query, near);
               if (r.rightTop() != null)
                   if (query.x() >= r.root().x())
                       near = searchNearest(r.rightTop(), query, near); 
           }          
       } else {
           if (Math.abs(r.root().y() - query.y()) < nearest.distanceSquaredTo(query)) {
               if (r.leftBottom() != null)
                   near = searchNearest(r.leftBottom(), query, near);
               if (r.rightTop() != null)
                   near = searchNearest(r.rightTop(), query, near);
           } else {
               if (r.leftBottom() != null)
                   if (query.y() < r.root().y())
                       near = searchNearest(r.leftBottom(), query, near);
               if (r.rightTop() != null)
                   if (query.y() >= r.root().y())
                       near = searchNearest(r.rightTop(), query, near); 
          }
       }
       } else {
           if (r.leftBottom() != null)
           if (r.leftBottom().getRect().distanceSquaredTo(query) < nearest.distanceSquaredTo(query))
               near = searchNearest(r.leftBottom(), query, near);
           if (r.rightTop() != null)
           if (r.rightTop().getRect().distanceSquaredTo(query) < nearest.distanceSquaredTo(query))
               near = searchNearest(r.rightTop(), query, near);
       }
           
       
       
       /*
       if (r.leftBottom() != null)
           if (r.leftBottom().getRect().distanceSquaredTo(query) < nearest.distanceSquaredTo(query))
               near = searchNearest(r.leftBottom(), query, near);
       if (r.rightTop() != null)
           if (r.rightTop().getRect().distanceSquaredTo(query) < nearest.distanceSquaredTo(query))
               near = searchNearest(r.rightTop(), query, near);
       */
       return near;
   }
   
   private Node put(Node r, Point2D p) {
       if (r == null) {
           Node ne = new Node(p);
           ne.setRect(0, 0, 1, 1);
           return ne;
       }
       if (p.x() < r.root().x()) {
           if (r.leftBottom() == null) {
               Node ne = new Node(p);
               ne.setRect(0, 0, r.root().x(), 1);
               ne.setxy(false);
               r.setLB(ne);
           } else {
              put(r, r.leftBottom(), p);
           }
       } else {
           if (r.rightTop() == null) {
               Node ne = new Node(p);
               ne.setRect(r.root().x(), 0, 1, 1);
               ne.setxy(false);
               r.setRT(ne);
           } else {
               put(r, r.rightTop(), p);
           }
       }
       return r;
   }
   
   private Node put(Node r, Node x, Point2D p) {
       Node n;  // pointer of next node
       if (x == null) {
           Node ne = new Node(p);
           setRect(r, ne);
           return ne;
       }
       if (x.xy()) {
           if (p.x() < x.root().x()) {
               n = put(x, x.leftBottom(), p);
               x.setLB(n);
           } else {
               n = put(x, x.rightTop(), p);
               x.setRT(n);
           }
           n.setxy(false);
           // resize rect
       } else {
           if (p.y() < x.root().y()) {
               n = put(x, x.leftBottom(), p);
               x.setLB(n);
           } else {
               n = put(x, x.rightTop(), p);
               x.setRT(n);
           }
           n.setxy(true);
       }
       return x;
   }
   
   private void setRect(Node r, Node next) {
       if (next == null) return;
       double xmin = r.getRect().xmin();
       double ymin = r.getRect().ymin();
       double xmax = r.getRect().xmax();
       double ymax = r.getRect().ymax();
       if (r.xy()) {
           if (next.root().x() < r.root().x()) {
               next.setRect(xmin, ymin, r.root().x(), ymax);
           } else {
               next.setRect(r.root().x(), ymin, xmax, ymax);
           }
       } else {
           if (next.root().y() < r.root().y()) {
               next.setRect(xmin, ymin, xmax, r.root().y());
           } else {
               next.setRect(xmin, r.root().y(), xmax, ymax);
           }
       }
   }
   
   /*
    private void resize(Node root, Node next) {
        double xmin = root.getRect().xmin();
        double ymin = root.getRect().ymin();
        double xmax = root.getRect().xmax();
        double ymax = root.getRect().ymax();
        double nxmin = next.getRect().xmin();
        double nymin = next.getRect().ymin();
        double nxmax = next.getRect().xmax();
        double nymax = next.getRect().ymax();
        if (nxmin < xmin) xmin = nxmin;
        if (nymin < ymin) ymin = nymin;
        if (nxmax > xmax) xmax = nxmax;
        if (nymax > ymax) ymax = nymax;
        root.setRect(xmin, ymin, xmax, ymax);
    }
    */
    
    private static class Node {
        private Point2D p;      // the point
        private RectHV rect;    // the axis-aligned rectangle corresponding to this node
        private Node lb;        // the left/bottom subtree
        private Node rt;        // the right/top subtree
        private boolean xy;     // true for x, false for y
        
        public Node(Point2D p) {
            this.p = p;
            xy = true;
            rect = new RectHV(p.x(), p.y(), p.x(), p.y());
        }
        
        public Node leftBottom() {
            return lb;
        }
        
        public Node rightTop() {
            return rt;
        }
        
        public void setLB(Node x) {
            lb = x;
        }
        
        public void setRT(Node x) {
            rt = x;
        }
        
        public RectHV getRect() {
            return rect;
        }
        
        public void setRect(double xmin, double ymin, double xmax, double ymax) {
            rect = new RectHV(xmin, ymin, xmax, ymax);
        }
        
        public Point2D root() {
            return p;
        }
        
        public boolean xy() {
            return xy;
        }
        
        public void setxy(boolean col) {
            xy = col;
        }
    }
}
